//
//  SettingsViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 19/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    //Data
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var branchLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let preferences = UserDefaults.standard
        
        if(preferences.object(forKey: "Branch") != nil){
            if(preferences.string(forKey: "privilege") == "admin"){
                branchLabel.text = "Branch: ALL"
            } else {
                branchLabel.text = "Branch: "+preferences.string(forKey: "Branch")!
            }
        }
        if(preferences.object(forKey: "username") != nil){
            usernameLabel.text = "Username: "+preferences.string(forKey: "username")!
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: "username")
        preferences.removeObject(forKey: "privilege")
        preferences.removeObject(forKey: "Branch")
        preferences.removeObject(forKey: "Products")
        preferences.removeObject(forKey: "save")
        
        self.dismiss(animated: true, completion: {});
        self.navigationController?.popViewController(animated: true);
    }

}
