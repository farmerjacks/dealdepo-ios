//
//  ViewController.swift
//  AppBackgroundImgs
//
//  Created by Admin on 28/6/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit
import Contacts

class SignInViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate {
    var subjects = [String]()
    var messages = [String]()
    var intervals = [String]()
    
    var myPickerView : UIPickerView!
    @IBOutlet var pickerView: UIView!
    
    //Suppliers
    var suppliers = [String]()
    var supIndex = 0
    //Activity Indiciator
    var progress = ProgressHUD(text: "Signing In")
    //UI Elements
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    let myGroup = DispatchGroup()

    override func viewDidLoad() {
        //Tap event on picker view
        /*let pickerTap = UITapGestureRecognizer(target: self, action: #selector(self.pickerTapped(tapRecognizer:)))
        pickerTap.cancelsTouchesInView = false
        pickerTap.delegate = self
        picker.addGestureRecognizer(pickerTap)*/
        
        GetNotify()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 44, width: self.view.frame.size.width, height: 326))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        
        // ToolBar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 44))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(SignInViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(SignInViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerView.addSubview(toolBar)
        pickerView.addSubview(self.myPickerView)
    }
    
    //MARK:- Button
    @objc func doneClick() {
        pickerView.isHidden = true
    }
    @objc func cancelClick() {
        pickerView.isHidden = true
    }
    
    //Tap event on pickerview
    @objc func pickerTapped(tapRecognizer:UITapGestureRecognizer) {
//        if (tapRecognizer.state == UIGestureRecognizerState.ended) {
//            let rowHeight : CGFloat  = self.picker.rowSize(forComponent: 0).height
//            let selectedRowFrame: CGRect = self.picker.bounds.insetBy(dx: 0.0, dy: (self.picker.frame.height - rowHeight) / 2.0 )
//            let userTappedOnSelectedRow = (selectedRowFrame.contains(tapRecognizer.location(in: picker)))
//            if (userTappedOnSelectedRow) {
//                let row = self.picker.selectedRow(inComponent: 0)
//                supIndex = row
//                supplierButton.setTitle(suppliers[row], for: .normal)
//                UserDefaults.standard.set(String(describing: suppliers[row]), forKey: "Supplier")
//                picker.isHidden = trueå
//            }
//        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @IBOutlet weak var supplierButton: UIButton!
    
    @IBAction func supplierAction(_ sender: Any) {
        if(pickerView.isHidden == true){
            pickerView.isHidden = false
        } else {
            pickerView.isHidden = true
        }
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        GetSuppliers()
        let preferences = UserDefaults.standard
        
        if(preferences.object(forKey: "privilege") != nil) {
            let priv = preferences.string(forKey: "privilege") as String?
            LoginDone(priv: priv!)
        }
        
    }
    
    @IBAction func Login(_ sender: Any) {
        let username = self.username.text
        let password = self.password.text
        if(username == "" || password == "") {
            Message(msg: "Username or password are empty!")
        } else {
            DoLogin(username: username!, password: password!)
        }
        
    }
    
    func DoLogin(username: String, password : String){
        if Reachability.isConnectedToNetwork() == true{
            self.view.addSubview(progress)
            self.progress.show()
            let url = URL(string: Constants.URLS.BASE+"/login?username="+username+"&password="+password+"&"+Constants.URLS.APIKEY)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Item"] as? [String: Any] {
                        let preferences = UserDefaults.standard
                        
                        preferences.set(String(describing: items["username"]!), forKey: "username")
                        preferences.set(String(describing: items["privilege"]!), forKey: "privilege")
                        preferences.set(String(describing: items["Branch"]!), forKey: "Branch")
                        preferences.set(String(describing: items["StoreCode"]!), forKey: "StoreCode")
                        
                        //self.LoginDone(priv: String(describing: items["privilege"]!))
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            let preferences = UserDefaults.standard
                            let priv = preferences.string(forKey: "privilege") as String?
                            
                            if(priv != nil){
                                self.LoginDone(priv: priv!)
                            } else {
                                self.Message(msg: "Username or password are invalid!")
                            }
                        })
                        
                    } else {
                        self.progress.hide()
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            let preferences = UserDefaults.standard
                            let priv = preferences.string(forKey: "privilege") as String?
                            
                            if(priv != nil){
                                self.LoginDone(priv: priv!)
                            } else {
                                self.Message(msg: "Username or password are invalid!")
                            }
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func GetNotify(){
        if Reachability.isConnectedToNetwork() == true{
            let url = URL(string: Constants.URLS.BASE+Constants.URLS.METHOD+"Notifications&"+Constants.URLS.APIKEY)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                if(data != nil){
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            
                            if let i = item["Subject"] as? String {
                                self.subjects.append(i)
                            }
                            if let i = item["Message"] as? String {
                                self.messages.append(i)
                            }
                            if let i = item["Interval"] as? String {
                                self.intervals.append(i)
                            }
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            let preferences = UserDefaults.standard
                            if(self.subjects.count > 0){
                                preferences.set(self.subjects, forKey: "subjects")
                                preferences.set(self.messages, forKey: "messages")
                                preferences.set(self.intervals, forKey: "intervals")
                                self.Notify()
                            } else {
                                preferences.removeObject(forKey: "subjects")
                                preferences.removeObject(forKey: "messages")
                                preferences.removeObject(forKey: "intervals")
                            }
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func GetSuppliers(){
        self.suppliers = []
        if Reachability.isConnectedToNetwork() == true{
            let url = URL(string:Constants.URLS.BASE+Constants.URLS.GET_SUPPLIER)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                if(data != nil){
                    do {
                        if let data = data,
                            let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                            let items = json["Items"] as? [[String: Any]] {
                            for item in items {
                                
                                if let i = item["SupplierName"] as? String {
                                    self.suppliers.append(i)
                                }
                            }
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.supplierButton.setTitle(self.suppliers[self.supIndex], for: .normal)
                                self.myPickerView.reloadAllComponents()
                                UserDefaults.standard.set(String(describing: self.suppliers[0]), forKey: "Supplier")
                            })
                        }
                        
                    } catch {
                        print("Error deserializing JSON: \(error)")
                    }
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func Notify(){
        var alerts = [UIAlertController]()
        alerts = []
        let date = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.weekday, from: date)
        let index = day-1
        
        
        var msg = UserDefaults.standard.object(forKey: "messages") as? [String]
        var sub = UserDefaults.standard.object(forKey: "subjects") as? [String]
        var intervals = UserDefaults.standard.object(forKey: "intervals") as? [String]
        
        var msgCount = 0
        
        let len = msg?.count
        if(len! > 0){
            for i in 0..<len! {
                var intv = intervals?[i].components(separatedBy: "-")
                if(intv?[index] == "true"){

                    let alert = UIAlertController(title: sub?[i], message: msg?[i], preferredStyle: UIAlertController.Style.alert)
                    
                    alerts.append(alert)
                    msgCount+=1
                }
            }
        }
        //print(msgCount)
        UserDefaults.standard.set(msgCount, forKey: "msgCount")
        var action = UIAlertAction()
        let length = alerts.count
        var nextIndex = 0
        for i in 0..<length {
            action = UIAlertAction(title: "OK", style: .default) { (action) in
                nextIndex = i+1
                if(alerts.indices.contains(nextIndex) == true){
                    self.present(alerts[nextIndex], animated: true, completion: nil)
                } else {
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            alerts[i].addAction(action)
        }
        if(alerts.count > 0){
            self.present(alerts[0], animated: true, completion: nil)
        }
        
        UserDefaults.standard.set(true, forKey: "notified")
    }
    
    func LoginToDo() {
        //username.text = ""
        //password.text = ""
        Message(msg: "Username or Password invalid")
    }
    
    func LoginDone(priv: String) {
        self.performSegue(withIdentifier: "loginSegue", sender: Any?.self)
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return suppliers[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return suppliers.count
    }
    
    /*@objc(pickerView:didSelectRow:inComponent:) func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        supIndex = row
        supplierButton.setTitle(suppliers[row], for: .normal)
        UserDefaults.standard.set(String(describing: suppliers[row]), forKey: "Supplier")
        picker.isHidden = true
    }*/
}

