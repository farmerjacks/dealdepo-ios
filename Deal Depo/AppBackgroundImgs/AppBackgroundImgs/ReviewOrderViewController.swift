//
//  ReviewOrderViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 19/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class ReviewOrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate  {
    var weekday = ["SU", "M", "TU", "W", "TH", "F", "SA"]
    var currentIndex = 0
    var saved = false
    //Activity Indiciator
    var progress = ProgressHUD(text: "Sending")
    
    @IBOutlet weak var curOrder: UILabel!
    
    var products = [PrductsViewController.ProductStruct()]
    var currentDay = ""
    var orderDay = String()
    
    var aislBayUpdateArray = [PrductsViewController.ProductStruct()]
    var tickReqArray = [PrductsViewController.ProductStruct()]
    
    
    var SKU = [String]()
    var storCodes = [String]()
    var productNames = [String]()
    var qty = [String]()
    
    @IBOutlet weak var orderReviewTableView: UITableView!
    
    //Submit Order
    @IBAction func submitOrder(_ sender: Any) {
        let expiryDate = Calendar.current.date(byAdding:
            .day, // updated this params to add hours
            value: 7,
            to: Date())
        
        let order = GetOrder(expiryDate: expiryDate!.timeIntervalSince1970)
        let aislebay = GetAisleBay(expiryDate: expiryDate!.timeIntervalSince1970)
        let tickets = GetTickets(expiryDate: expiryDate!.timeIntervalSince1970)
        
        if(order != ""){
            self.view.addSubview(self.progress)
            self.progress.show()
            Save()
            SendOrder(order: order, aisleBayUpdate: aislebay, ticketsRequired: tickets)
        } else {
            self.Message(msg: "Order empty")
        }
    }
    
    func SendOrder(order: String, aisleBayUpdate: String, ticketsRequired: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            let json: [String: Any] = ["ticketsRequired": ticketsRequired,
                                       "aisleBayUpdate": aisleBayUpdate,
                                       "order": order,
                                       "apiKey": Constants.URLS.APIKEY2]
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/order");
            
            var request = URLRequest(url:myUrl!)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                self.progress.show()
                if error != nil
                {
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                        self.progress.hide()
                    }
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        if(items["Message"] != nil)
                        {
                            msg = items["Message"] as! String
                        }
                        else if(items["errorMessage"] != nil)
                        {
                            msg = items["errorMessage"] as! String
                        }
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            if(msg != "Error"){
                                UserDefaults.standard.set(true, forKey: "OrderSent")
                            }
                            self.MessageBack(msg: msg)
                            self.progress.hide()
                        })
                    } else {
                        print("Error!!")
                        self.progress.hide()
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        }
        else{
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func GetOrder(expiryDate: Double) ->String{
        let preferences = UserDefaults.standard
        
        var str = ""
        var storeCode = ""
        if(!preferences.string(forKey: "StoreCode")!.isEmpty){
            storeCode = preferences.string(forKey: "StoreCode")!
        }
        
        let username = preferences.string(forKey: "username")!
        let branch = preferences.string(forKey: "Branch")!
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy-HH:mm:ss"
        
        let now = formatter.string(from: date)
        
        if(products.count > 0){
            for item in products{
                if(Int(item.getQty)! > 0){
                    str += item.SKU+","
                    str += storeCode+","
                    str += item.getQty+","
                    str += now+","
                    str += branch+","
                    str += username+","
                    str += item.description+","
                    str += String(format:"%f", expiryDate)+","
                    str += orderDay+","
                    str += item.aisleBay+";"
                }
            }
        }
        
        return str
    }
    
    func GetAisleBay(expiryDate: Double) -> String{
        let preferences = UserDefaults.standard
        
        var str = ""
        
        let branch = preferences.string(forKey: "Branch")!
        if(saved == false){
        if(aislBayUpdateArray.count > 0){
            for item in aislBayUpdateArray{
                if(item.abUpdate == true){
                    str += branch+","
                    str += item.SKU+","
                    str += item.description+","
                    str += item.ChangeAisle+"-"+item.ChangeBay+","
                    str += "false,"
                    str += String(format:"%f", expiryDate)+";"
                }
            }
        }
        }
        
        if(str == ""){
            str = "NULL"
        }
        
        return str
    }
    
    func GetTickets(expiryDate: Double) -> String{
        let preferences = UserDefaults.standard
        
        var str = ""
        
        let branch = preferences.string(forKey: "Branch")!
        if(saved == false){
        if(tickReqArray.count > 0){
            for item in tickReqArray{
                if(item.tickReq == true){
                    str += branch+","
                    str += item.SKU+","
                    str += item.description+","
                    str += item.tickReqNum+","
                    str += "false,"
                    str += String(format:"%f", expiryDate)+";"
                }
            }
        }
        }
        if(str == ""){
            str = "NULL"
        }
        return str
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.weekday, from: date)
        currentDay = weekday[day-1]
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "RESUME ORDER", style: .plain, target: self, action: #selector(onClickBack))
        
        GenerateOrder()
        orderReviewTableView.reloadData()
        
        orderReviewTableView.dataSource = self
        orderReviewTableView.delegate = self
        
        let nibName = UINib(nibName: "OldOrderCell", bundle: nil)
        orderReviewTableView.register(nibName, forCellReuseIdentifier: "OldCell")
        
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(tapRecognizer:)))
        tapGR.delegate = self
        tapGR.numberOfTapsRequired = 2
        orderReviewTableView.addGestureRecognizer(tapGR)
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destView = segue.destination as! PrductsViewController
        destView.currentIndex = currentIndex
    }
    
    @objc func onClickBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTap(tapRecognizer:UITapGestureRecognizer){
        self.navigationController?.popViewController(animated: true)
    }
    
    func GenerateOrder() {
        CheckForNull()
        var error = ""
        if(products.count > 0){
            for item in products{
                if(Int(item.getQty) != nil) {
                    if(Int(item.getQty)! > 0){
                        SKU.append(item.SKU)
                        productNames.append(item.description)
                        qty.append(item.getQty)
                    }
                    if(item.abUpdate == true){
                        aislBayUpdateArray.append(item)
                    }
                    if(item.tickReq == true){
                        tickReqArray.append(item)
                    }
                } else {
                    error = "Product Qty is null"
                }
            }
        } else {
            Message(msg: "No Products")
        }
        
        if(error != ""){
            Message(msg: error)
        }
    }
    
    func CheckForNull(){
        let len = products.count
        
        for i in 0..<len{
            if(products[i].getQty == "NULL"){
                products[i].getQty = "0"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SKU.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderReviewTableView.dequeueReusableCell(withIdentifier: "OldCell", for: indexPath) as! OldOrderCell
        cell.commonInit(SKU[indexPath.item], tempProdName: productNames[indexPath.item], tempQty: qty[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(SKU[indexPath.row], forKey: "savedIndex")
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }*/
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func MessageBack(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func Save(){
        if(saved == false){
            saved = true
            let preferences = UserDefaults.standard
            
            preferences.set(self.currentIndex, forKey: "save")
            
            preferences.set(self.SaveProducts(), forKey: "Products")
            preferences.synchronize()
            //Message(msg: "Successfully Saved")
        }
    }
    
    func SaveProducts() -> [[String]]{
        var array = [[String]]()
        for item in products{
            if(Int(item.getQty)! > 0){
                var prod = [String]()
                prod.append(item.SKU)
                prod.append(item.getQty)
                prod.append(item.tickReqNum)
                if(item.tickReq == true){
                    prod.append("true")
                } else {
                    prod.append("false")
                }
                prod.append(item.ChangeAisle)
                prod.append(item.ChangeBay)
                if(item.abUpdate == true){
                    prod.append("true")
                } else {
                    prod.append("false")
                }
                prod.append(orderDay)
                
                array.append(prod)
            }
        }
        
        return array
    }
}
