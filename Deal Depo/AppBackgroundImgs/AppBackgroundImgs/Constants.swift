
//
//  Constants.swift
//  FarmerJacksApp
//
//  Created by Ahmed Sadiq on 29/6/19.
//  Copyright © 2019 SCS. All rights reserved.
//

import UIKit

class Constants: NSObject {
    struct URLS {
        static let BASE = "https://yhh3wx4yf8.execute-api.us-east-1.amazonaws.com/prod"
        static let GET_SUPPLIER = "/DBAccess?params=Suppliers&apiKey=7t82ufjhsf92uwh9d2"
        static let APIKEY = "apiKey=7t82ufjhsf92uwh9d2"
        static let APIKEY2 = "7t82ufjhsf92uwh9d2"
        static let METHOD = "/DBAccess?params="
        static let TABLE_IMAGES = "Images"
        static let TABLE_PRINT_TABLE = "PrintTicket"
        static let TABLE_REMOVE_TABLE = "RemoveTicket"
        static let TABLE_UNSTOCKED_PRODUCTS = "UnstockedProducts"
        
    }
    struct ERRORS {
        static let NO_INTERNET_CONNECTION = "Internet Connection not Available!"
        static let LOADING_NOTIFICATIONS = "Loading Notification"
        static let LOADING_TICKETS = "Loading Tickets"
        
    }
}
