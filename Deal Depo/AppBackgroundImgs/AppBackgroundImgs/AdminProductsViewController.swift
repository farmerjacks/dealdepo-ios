//
//  AdminProductsViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 26/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class AdminProductsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    //Image Picker
    var imagePicker: UIImagePickerController!
    
    var branch = "BUTLER"
    var selSupplier = "Deal Depot"
    var storeCode = "BUT"
    
    @IBOutlet weak var productImage: UIImageView!
    ///Products array;
    var products = [ProductStruct]()
    //Product Struct
    struct ProductStruct {
        var SKU: String = "00000"
        var aisleBay: String = "0000"
        var caseQty: String = "0000"
        var description: String = "0000"
        var gp: String = "0000"
        var metrics: String = "0000"
        var qty: String = "0000"
        var retailPrice: String = "0000"
        var saleDay: String = "0000"
        var supplier: String = "0000"
        var unitPrice: String = "0000"
        var avSales: String = "0000"
        var lastSales: String = "0000"
        var getQty: String = "0"
        var abUpdate: Bool = false
        var ChangeAisle = "00"
        var ChangeBay = "00"
        var tickReq: Bool = false
        var tickReqNum: String = "0"
        var image: String = ""
    }
    
    @IBOutlet weak var aisleBayButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    //Current Product index
    var currentIndex = 0
    
    //Activity Indiciator
    var progress = ProgressHUD(text: "Loading Products")
    
    //Product Labels
    @IBOutlet weak var SKU: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var supplier: UILabel!
    @IBOutlet weak var yestSale: UILabel!
    @IBOutlet weak var avSales: UILabel!
    @IBOutlet weak var lastWeekSales: UILabel!
    @IBOutlet weak var unitCost: UILabel!
    @IBOutlet weak var normalSell: UILabel!
    @IBOutlet weak var caseQty: UILabel!
    @IBOutlet weak var currentGP: UILabel!
    @IBOutlet weak var aisle: UIButton!
    
    //Aisle and Bay arrays
    var aisleBayArray = [String]()
    //Picker view
    @IBOutlet weak var picker: UIPickerView!
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return aisleBayArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return aisleBayArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let i = products.firstIndex { $0.aisleBay == aisleBayArray[row] }
        currentIndex = i!;
        self.SetData()
        self.picker.isHidden = true
    }
    
    
    
    @IBAction func next(_ sender: Any) {
        if(currentIndex < products.count-1){
            currentIndex += 1
            SetData()
        }
    }
    @IBAction func back(_ sender: Any) {
        if(currentIndex > 0){
            currentIndex -= 1
            SetData()
        }
    }
    
    //Change Aisle and bay
    @IBAction func ChangeAisle(_ sender: Any) {
        SetAisleBayData()
        picker.reloadAllComponents()
        picker.isHidden = false
    }
    
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editButton   = UIBarButtonItem(title: "TAKE PHOTO",  style: .plain, target: self, action: #selector(didTapDoneButton))
        
        self.navigationItem.rightBarButtonItem = editButton
        
        picker.isHidden = true
        //Add text change listener
        SetUserData()
    }
    
    //Take Photo
    @objc func didTapDoneButton(){
        if !UIImagePickerController.isSourceTypeAvailable(.camera){
            
            let alertController = UIAlertController.init(title: nil, message: "Device has no camera.", preferredStyle: .alert)
            
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
            })
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
        
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        imagePicker.dismiss(animated: true, completion: nil)
        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as! UIImage
        
        //Now use image to create into NSData format
        let imageData:NSData = image.pngData()! as NSData
        
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        let str = products[currentIndex].SKU+","+strBase64
        
        SendData(str: str)
    }
    
    func SendData(str: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            let json: [String: Any] = ["values": str,
                                       "table": "Images"]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/write");
            
            var request = URLRequest(url:myUrl!)
            
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                self.progress.show()
                if error != nil
                {
                    return
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        self.progress.hide()
                        if(items["Message"] != nil){
                            msg = items["Message"] as! String
                        } else {
                            msg = "Error!"
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            self.Message(msg: msg)
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetAisleBayData(){
        aisleBayArray = [String]()
        var temp = ""
        for item in products {
            if(temp == "" || temp != item.aisleBay){
                if(item.aisleBay != "NULL"){
                    temp = item.aisleBay
                    aisleBayArray.append(item.aisleBay)
                }
            }
        }
    }
    
    func SetData(){
        if(currentIndex < products.count){
            self.SKU.text = products[currentIndex].SKU
            self.supplier.text = products[currentIndex].supplier
            self.name.text = products[currentIndex].description
            self.lastWeekSales.text = products[currentIndex].lastSales
            self.avSales.text = products[currentIndex].avSales
            self.yestSale.text = products[currentIndex].qty
            self.unitCost.text = products[currentIndex].unitPrice
            self.normalSell.text = products[currentIndex].retailPrice
            self.currentGP.text = products[currentIndex].gp
            var aisleBay = products[currentIndex].aisleBay.components(separatedBy: "-")
            if(aisleBay.count > 1){
                self.aisle.setTitle("Aisle "+aisleBay[0] + " Bay " + aisleBay[1], for: .normal)
            }
            
            if(products[currentIndex].image != ""){
                if let decodedData = Data(base64Encoded: products[currentIndex].image, options: .ignoreUnknownCharacters) {
                    let image = UIImage(data: decodedData)
                    productImage.image = image
                }
            }
        }
    }
    
    func GetProducts(table: String, branch : String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let url = URL(string: Constants.URLS.BASE+Constants.URLS.METHOD+"admin")
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = ProductStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["aisleBay"] as? String {
                                struc.aisleBay = i
                            }
                            if let i = item["caseQty"] as? String {
                                struc.caseQty = i
                            }
                            if let i = item["description"] as? String {
                                struc.description = i
                            }
                            if let i = item["gp"] as? String {
                                struc.gp = i
                            }
                            if let i = item["metrics"] as? String {
                                struc.metrics = i
                            }
                            if let i = item["qty"] as? String {
                                struc.qty = i
                            }
                            if let i = item["retailPrice"] as? String {
                                struc.retailPrice = i
                            }
                            if let i = item["saleDay"] as? String {
                                struc.saleDay = i
                            }
                            if let i = item["supplier"] as? String {
                                struc.supplier = i
                            }
                            if let i = item["unitPrice"] as? String {
                                struc.unitPrice = i
                            }
                            if let i = item["AvSales"] as? String {
                                struc.avSales = i
                            }
                            if let i = item["lastSales"] as? String {
                                struc.lastSales = i
                            }
                            if(item["Image"] != nil){
                                if let i = item["Image"] as? String {
                                    struc.image = i
                                }
                            }
                            self.products.append(struc)
                        }
                    } else {
                        print("Error!!")
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.SetData()
                    self.progress.hide()
                    self.aisleBayButton.isHidden = false
                    self.mainView.isHidden = false
                    self.products = self.products.sorted(by: { $0.aisleBay < $1.aisleBay })
                    if(!self.products.isEmpty){
                        self.SetData()
                    }
                })
            }
            task.resume()
            
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    @IBOutlet var currentView: UIView!
    
    func activityIndicator() {
        self.view.addSubview(self.progress)
        self.aisleBayButton.isHidden = true
        self.mainView.isHidden = true
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "StoreCode")!.isEmpty){
            storeCode = preferences.string(forKey: "StoreCode")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            selSupplier = preferences.string(forKey: "Supplier")!
        }
        
        let save = preferences.integer(forKey: "save")
        currentIndex = save
        
        if(preferences.object(forKey: "Products") != nil){
            products = preferences.object(forKey: "Products")! as! [ProductStruct]
        } else {
            GetProducts(table: "Products", branch: branch)
        }
        
        self.SetData()
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
