//
//  DispatcherProtocol.swift
//  FarmerJacksApp
//
//  Created by Ahmed Sadiq on 9/7/19.
//  Copyright © 2019 SCS. All rights reserved.
//

import UIKit

class DispatcherProtocol: NSObject {
    
}

//The dispatcher is responsible to execute a Request and provide the response
public protocol Dispatcher {
    
    //Configure the dispatcher with an environment
    init(environment: Environment)
    
    //This function executes the request and provide a response
    func execute(request: Request, completion: @escaping (Any?) -> ()) throws
}
