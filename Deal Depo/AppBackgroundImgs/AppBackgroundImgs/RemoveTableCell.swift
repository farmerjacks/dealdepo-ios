//
//  RemoveTableCell.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class RemoveTableCell: UITableViewCell {
    
    @IBOutlet weak var SKU: UILabel!
    @IBOutlet weak var aisleBay: UILabel!
    @IBOutlet weak var active: UISwitch!
    
    @IBOutlet weak var prodName: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(_ tempSKU: String, tempProdName: String, tempAisleBay: String, tempActive: Bool){
        SKU.text = tempSKU
        prodName.text = tempProdName
        aisleBay.text = tempAisleBay
        
        print(tempActive)
        if(tempActive == true){
            self.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            self.accessoryType = UITableViewCell.AccessoryType.none
        }
        
    }
}
