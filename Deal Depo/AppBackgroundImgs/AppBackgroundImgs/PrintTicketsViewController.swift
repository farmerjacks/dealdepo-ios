//
//  PrintTicketsViewController.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class PrintTicketsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var PrintTicketsTableView: UITableView!
    var checkChange = false
    var backSelect = false
    var branch = "BUTLER"
    var supplier = "Deal Depot"
    
    //Print Ticket struct
    struct PrintStruct {
        var SKU: String = "N/A"
        var description: String = "N/A"
        var aislBay: String = "N/A"
        var qty: String = "N/A"
        var checked: Bool = false
    }
    
    var tickets = [PrintStruct]()
    var sendTickets = [PrintStruct]()
    
    var SKU = [String]()
    var productNames = [String]()
    var aisleBay = [String]()
    var qty = [String]()
    var active = [Bool]()
    
    let tickSwitch = UISwitch()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editButton   = UIBarButtonItem(title: "SAVE",  style: .plain, target: self, action: #selector(didTapDoneButton))
        
        tickSwitch.addTarget(self, action: #selector(TickAllFunc), for: .valueChanged)
        
        let tickButton   = UIBarButtonItem(customView: tickSwitch)
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "BACK", style: .plain, target: self, action: #selector(onClickBack))
        
        self.navigationItem.rightBarButtonItems = [editButton, tickButton]
        
        SetUserData()
        
        GetData(table: "PrintTicket", branch : branch)
        
        PrintTicketsTableView.dataSource = self
        PrintTicketsTableView.delegate = self
        
        
        let nibName = UINib(nibName: "PrintTableCell", bundle: nil)
        PrintTicketsTableView.register(nibName, forCellReuseIdentifier: "PrintCell")
        // Do any additional setup after loading the view.
    }
    
    @objc func onClickBack(){
        if(checkChange == true){
            CheckMessage()
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func CheckMessage() {
        let alert = UIAlertController(title: "Alert", message: "There are unsaved tickets do you wish to save it?", preferredStyle: UIAlertController.Style.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            let tickets = self.GetTickets()
            if(tickets != "NULL"){
                self.backSelect = true
                self.SendData(tickets: tickets)
            }
        }
        let noAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func didTapDoneButton(sender: AnyObject) {
        let tickets = GetTickets()
        if(tickets != "NULL"){
            SendData(tickets: tickets)
        }
    }
    
    func GetTickets() -> String{
        let preferences = UserDefaults.standard
        
        var str = ""
        
        let branch = preferences.string(forKey: "Branch")!
        
        if(sendTickets.count > 0){
            for item in sendTickets{
                str += branch+","
                str += item.SKU+","
                if(item.checked == true){
                    str += "true;"
                } else {
                    str += "false;"
                }
                
            }
        }
        if(str == ""){
            str = "NULL"
        }
        return str
    }
    
    @objc func TickAllFunc() {
        let len = active.count
        if(len > 0){
            if(tickSwitch.isOn == true){
                for i in 0..<len {
                    active[i] = true
                    tickets[i].checked = true
                    sendTickets.append(tickets[i])
                }
            } else {
                for i in 0..<len {
                    active[i] = false
                    tickets[i].checked = false
                }
                sendTickets.removeAll()
            }
        }
        checkChange = true
        PrintTicketsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SKU.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = PrintTicketsTableView.dequeueReusableCell(withIdentifier: "PrintCell", for: indexPath) as! PrintTableCell
        cell.commonInit(SKU[indexPath.item], tempProdName: productNames[indexPath.item], tempQty: qty[indexPath.item], tempActive: active[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView.cellForRow(at: indexPath)?.accessoryType == UITableViewCell.AccessoryType.checkmark){
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.none
            
            active[indexPath.item] = false
            tickets[indexPath.item].checked = false
            
            if let location = sendTickets.firstIndex(where: { $0.SKU == tickets[indexPath.item].SKU }) {
                sendTickets.remove(at: location)
            }
        } else {
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
            
            active[indexPath.item] = true
            tickets[indexPath.item].checked = true
            
            sendTickets.append(tickets[indexPath.item])
        }
        checkChange = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetData(){
        SKU = [String]()
        productNames = [String]()
        aisleBay = [String]()
        qty = [String]()
        active = [Bool]()
        for item in tickets{
            SKU.append(item.SKU)
            productNames.append(item.description)
            aisleBay.append(item.aislBay)
            qty.append(item.qty)
            active.append(item.checked)
        }
        PrintTicketsTableView.reloadData()
    }
    

    func GetData(table: String, branch : String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch+"&"+Constants.URLS.APIKEY
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                if(data == nil){
                    self.progress.hide()
                }
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                        self.progress.hide()
                    }
                }
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = PrintStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["aisleBay"] as? String {
                                if(i == "NULL"){
                                    struc.aislBay = "N/A"
                                } else {
                                    struc.aislBay = i
                                }
                            }
                            if let i = item["flag"] as? String {
                                if(i == "false"){
                                    struc.checked = false
                                } else {
                                    struc.checked = true
                                }

                            }
                            if let i = item["value"] as? String {
                                struc.qty = i
                            }
                            if let i = item["description"] as? String {
                                if(i == "NULL"){
                                    struc.description = "N/A"
                                } else {
                                    struc.description = i
                                }
                            }
                            
                            self.tickets.append(struc)
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.SortProducts() 
                            self.SetData()
                            self.progress.hide()
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()

        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    //Activity Indiciator
    var progress = ProgressHUD(text: "Saving")
    
    func SendData(tickets: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            self.progress = ProgressHUD(text: "Sending")
            self.view.addSubview(self.progress)
            self.progress.show()
            
            let json: [String: Any] = ["values": tickets,
                                       "table": Constants.URLS.TABLE_PRINT_TABLE,
                                       "apiKey": Constants.URLS.APIKEY2]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/write");
            
            var request = URLRequest(url:myUrl!)
            
            request.setValue("text/json", forHTTPHeaderField: "Content-Type")
            
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                self.progress.show()
                if error != nil
                {
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        
                        if(items["Message"] != nil){
                            msg = items["Message"] as! String
                        } else {
                            msg = "Error!"
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            self.DeleteTickets()
                            self.checkChange = false
                            if(self.backSelect == true){
                                self.MessageOther(msg: msg)
                            } else {
                                self.Message(msg: msg)
                            }
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
        
    }
    
    func DeleteTickets(){
        for item in sendTickets{
            if(item.checked == true){
                if let location = tickets.firstIndex(where: { $0.SKU == item.SKU }) {
                    tickets.remove(at: location)
                }
            }
        }
        self.SetData()
        sendTickets = []
        self.PrintTicketsTableView.reloadData()
    }

    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func MessageOther(msg: String){
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        let noAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func activityIndicator() {
        progress = ProgressHUD(text: "Loading Tickets")
        self.view.addSubview(self.progress)
        progress.show()
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            supplier = preferences.string(forKey: "Supplier")!
        }
    }
    
    //Sort Products by ailse and bay then alpha
    func SortProducts(){
        self.tickets = self.tickets.sorted(by: { $0.aislBay < $1.aislBay })
        var current = ""
        var prod = [PrintStruct()]
        prod = []
        var array = [PrintStruct()]
        array = []
        for item in self.tickets{
            if(current == ""){
                current = item.aislBay
            }
            
            if(current != item.aislBay){
                current = item.aislBay
                array = array.sorted(by: { $0.description < $1.description })
                prod.append(contentsOf: array)
                array = []
            }
            
            array.append(item)
        }
        prod.append(contentsOf: array)
        
        self.tickets = prod
    }
}
