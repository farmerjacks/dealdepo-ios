//
//  APIRequest.swift
//  FarmerJacksApp
//
//  Created by Ahmed Sadiq on 9/7/19.
//  Copyright © 2019 SCS. All rights reserved.
//

import UIKit

public enum APIRequest {
    case login(username: String, password: String)
    case logout
}

extension APIRequest: Request {
    
    public var path: String {
        switch self {
        case .login( _, _):
            return "/users/login"
        case .logout:
            return "/users/logout"
        }
    }
    
    public var parameters: RequestParams {
        switch self {
        case .login(let username, let password):
            return .body(["user" : username, "password" : password])
        case .logout:
            return.body([:])
        }
    }
    
    public var headers: [String : Any]? {
        return [:]
    }
    
    public var method: HTTPMethod {
        switch self {
        case .login(_,_):
            return .post
        case .logout:
            return .post
        }
    }
    
    public var dataType: DataType {
        switch self {
        case .login(_,_):
            return .Json
        case .logout:
            return .Json
        }
    }
}
