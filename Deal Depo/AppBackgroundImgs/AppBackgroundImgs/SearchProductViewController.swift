//
//  SearchProductViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 26/7/17.
//  Copyright © 2017 SCS, DanCom Software. All rights reserved.
//

import UIKit

class SearchProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate {

    let uiBusy = UIActivityIndicatorView(style: .white)
    
    @IBOutlet weak var searchTable: UITableView!
    
    @IBOutlet weak var picker: UIPickerView!
    var branchIndex = 0
    var branches = [String]()
    
    var progress = ProgressHUD(text: "Loading")
    
    var SKU = [String]()
    var productNames = [String]()
    var aisleBay = [String]()
    var qty = [String]()
    
    var supplier = "Deal Depo"
    
    var currentIndex = 0
    var selectedProducts = [PrductsViewController.ProductStruct()]
    
    var products = [PrductsViewController.ProductStruct()]
    var searchProducts = [PrductsViewController.ProductStruct()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Tap event on picker view
        let pickerTap = UITapGestureRecognizer(target: self, action: #selector(self.pickerTapped(tapRecognizer:)))
        pickerTap.cancelsTouchesInView = false
        pickerTap.delegate = self
        picker.addGestureRecognizer(pickerTap)
        
        let preferences = UserDefaults.standard
        var priv = "employee"
        
        if(preferences.object(forKey: "privilege") != nil) {
            priv = (preferences.string(forKey: "privilege") as String?)!
        }
        if(priv == "admin"){
            GetBranches()
        } else {
            var branch = ""
            
            if(!preferences.string(forKey: "Branch")!.isEmpty){
                branch = preferences.string(forKey: "Branch")!
            }
            
            if(!preferences.string(forKey: "Supplier")!.isEmpty){
                self.supplier = preferences.string(forKey: "Supplier")!
            }
            self.GetProducts(table: "SearchProducts", branch : branch)
        }

        let nibName = UINib(nibName: "RemoveTableCell", bundle: nil)
        searchTable.register(nibName, forCellReuseIdentifier: "RemoveCell")
        
        // Do any additional setup after loading the view.
    }
    
    //Tap event on pickerview
     @objc func pickerTapped(tapRecognizer:UITapGestureRecognizer) {
        if (tapRecognizer.state == UIGestureRecognizer.State.ended) {
            let rowHeight : CGFloat  = self.picker.rowSize(forComponent: 0).height
            let selectedRowFrame: CGRect = self.picker.bounds.insetBy(dx: 0.0, dy: (self.picker.frame.height - rowHeight) / 2.0 )
            let userTappedOnSelectedRow = (selectedRowFrame.contains(tapRecognizer.location(in: picker)))
            if (userTappedOnSelectedRow) {
                let row = self.picker.selectedRow(inComponent: 0)
                branchIndex = row
                picker.isHidden = true
                self.GetProducts(table: "SearchProducts", branch : self.branches[self.branchIndex])
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func ShowList(){
        if(picker.isHidden == true){
            picker.isHidden = false
        } else {
            self.GetProducts(table: "SearchProducts", branch : self.branches[self.branchIndex])
            picker.isHidden = true
        }
    }
    
    @IBOutlet weak var searchText: UITextField!
    @IBAction func SearchProduct(_ sender: Any) {
        let search = searchText.text
        
        if(search != ""){
            Search(val: search!)
        } else {
            SetAllData()
        }
    }
    
    func Search(val: String){
        searchProducts = []
        for item in products{
            if(item.SKU != "N/A"){
                if(item.description.lowercased().contains(val.lowercased()) || item.SKU.lowercased().contains(val.lowercased()) ||
                    item.aisleBay.lowercased().contains(val.lowercased())){
                    searchProducts.append(item)
                }
            }
        }
        
        if(searchProducts.count == 0){
            self.Message(msg: "Product not found")
        }
        
        SetData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let found = segue.destination as! FoundProductViewController
        found.product = selectedProducts
        found.index = currentIndex
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SKU.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchTable.dequeueReusableCell(withIdentifier: "RemoveCell", for: indexPath) as! RemoveTableCell
        
        cell.commonInit(SKU[indexPath.item], tempProdName: productNames[indexPath.item], tempAisleBay: aisleBay[indexPath.item], tempActive: false)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(searchProducts.count > 0){
            selectedProducts = searchProducts
        } else {
            selectedProducts = products
        }
        currentIndex = indexPath.item
        performSegue(withIdentifier: "searchProduct", sender: self)
        
    }
    
    func GetProducts(table: String, branch : String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let preferences = UserDefaults.standard
            var priv = "employee"
            if(preferences.object(forKey: "privilege") != nil) {
                priv = (preferences.string(forKey: "privilege") as String?)!
            }
            var urlStr = ""
            if(priv == "admin"){
                urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+","+branches[branchIndex]+"&"+Constants.URLS.APIKEY
            } else {
                urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch+","+self.supplier+"&"+Constants.URLS.APIKEY
            }
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        self.searchProducts = []
                        self.products = []
                        for item in items {
                            var struc = PrductsViewController.ProductStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["aisleBay"] as? String {
                                struc.aisleBay = self.ValidateAisleBay(aisleBay: i)
                            }
                            if let i = item["caseQty"] as? String {
                                struc.caseQty = i
                            }
                            if let i = item["description"] as? String {
                                struc.description = i
                            }
                            if let i = item["gp"] as? String {
                                struc.gp = i
                            }
                            if let i = item["metrics"] as? String {
                                struc.metrics = i
                            }
                            if let i = item["qty"] as? String {
                                struc.qty = i
                            }
                            if let i = item["retailPrice"] as? String {
                                struc.retailPrice = i
                            }
                            if let i = item["saleDay"] as? String {
                                struc.saleDay = i
                            }
                            if let i = item["supplier"] as? String {
                                struc.supplier = i
                            }
                            if let i = item["unitPrice"] as? String {
                                struc.unitPrice = i
                            }
                            if let i = item["AvSales"] as? String {
                                struc.avSales = i
                            }
                            if let i = item["lastSales"] as? String {
                                struc.lastSales = i
                            }
                            self.products.append(struc)
                        }
                    } else {
                        self.Message(msg: "Error!!")
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.progress.hide()
                    self.Find()
                    self.SortProducts()
                    self.SetAllData()
                })
            }
            task.resume()
            
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    //Validate Aisle and Bay
    func ValidateAisleBay(aisleBay: String) -> String{
        var value = aisleBay
        
        let letters = NSCharacterSet.letters
        let range = aisleBay.rangeOfCharacter(from: letters)
        
        if(aisleBay == "99-99"){
            value = "??-??"
        }
        
        if range != nil {
            value = "??-??"
        }
        
        return value
    }
    
    func GetBranches(){
        if Reachability.isConnectedToNetwork() == true{
            uiBusy.hidesWhenStopped = true
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: uiBusy)
            let preferences = UserDefaults.standard
            var priv = "employee"
            if(preferences.object(forKey: "privilege") != nil) {
                priv = (preferences.string(forKey: "privilege") as String?)!
            }
            var urlStr = ""
            if(priv == "admin"){
                urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+"Branches&"+Constants.URLS.APIKEY
            }
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    self.uiBusy.stopAnimating()
                    return
                }
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                        self.uiBusy.stopAnimating()
                    }
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        self.branches = []
                        for item in items {
                            if let i = item["Branch"] as? String {
                                self.branches.append(i)
                            }
                        }
                    } else {
                        self.Message(msg: "Error!!")
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.uiBusy.stopAnimating()
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "CHANGE BRANCH", style: .plain, target: self, action: #selector(self.ShowList))
                    self.picker.reloadAllComponents()
                    
                    if(!preferences.string(forKey: "Supplier")!.isEmpty){
                        self.supplier = preferences.string(forKey: "Supplier")!
                    }
                    self.GetProducts(table: "SearchProducts", branch : self.branches[self.branchIndex])
                })
            }
            task.resume()
            
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func SetData(){
        SKU = [String]()
        productNames = [String]()
        aisleBay = [String]()
        qty = [String]()
        for item in searchProducts{
            SKU.append(item.SKU)
            productNames.append(item.description)
            aisleBay.append(item.aisleBay)
            qty.append(item.qty)
        }
        searchTable.reloadData()
    }
    
    func SetAllData(){
        SKU = [String]()
        productNames = [String]()
        aisleBay = [String]()
        qty = [String]()
        for item in products{
            if(item.SKU != "N/A"){
                SKU.append(item.SKU)
                productNames.append(item.description)
                aisleBay.append(item.aisleBay)
                qty.append(item.qty)
            }
        }
        searchTable.reloadData()
    }
    
    func SortProducts(){
        self.products = self.products.sorted(by: { $0.aisleBay < $1.aisleBay })
        var current = ""
        var prod = [PrductsViewController.ProductStruct()]
        prod = []
        var array = [PrductsViewController.ProductStruct()]
        array = []
        for item in self.products{
            if(current == ""){
                current = item.aisleBay
            }
            
            if(current != item.aisleBay){
                current = item.aisleBay
                array = array.sorted(by: { $0.description < $1.description })
                prod.append(contentsOf: array)
                array = []
            }
            
            array.append(item)
        }
        prod.append(contentsOf: array)
        
        self.products = prod
    }
    
    func Find(){
        print(self.products.count)
    }

    func activityIndicator() {
        self.view.addSubview(self.progress)
        self.progress.show()
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return branches[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return branches.count
    }
    
    /*@objc(pickerView:didSelectRow:inComponent:) func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        branchIndex = row
        picker.isHidden = true
        self.GetProducts(table: "SearchProducts", branch : self.branches[self.branchIndex])
    }*/
    
    /*
     The MIT License (MIT)
     
     Copyright (c) 2015 Alex Littlejohn
     
     Permission is hereby granted, free of charge, to any person obtaining a copy
     of this software and associated documentation files (the "Software"), to deal
     in the Software without restriction, including without limitation the rights
     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
     copies of the Software, and to permit persons to whom the Software is
     furnished to do so, subject to the following conditions:
     
     The above copyright notice and this permission notice shall be included in all
     copies or substantial portions of the Software.
     
     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
     SOFTWARE.
     */

}
