//
//  UnstockTabelCell.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class UnstockTabelCell: UITableViewCell {

    @IBOutlet weak var mainSKU: UILabel!
    
    @IBOutlet weak var prodName: UITextView!
    
    @IBOutlet weak var aisleBay: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func commonInit(_ tempSKU: String, tempProdName: String, tempAisleBay: String, tempActive: Bool){
        mainSKU.text = tempSKU
        prodName.text = tempProdName
        aisleBay.text = tempAisleBay
    
        if(tempActive == true){
            self.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            self.accessoryType = UITableViewCell.AccessoryType.none
        }
    }
    
}
