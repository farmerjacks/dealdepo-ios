//
//  PrintTableCell.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class PrintTableCell: UITableViewCell {

    @IBOutlet weak var SKU: UILabel!
    @IBOutlet weak var prodName: UITextView!
    @IBOutlet weak var qty: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    func commonInit(_ tempSKU: String, tempProdName: String, tempQty: String, tempActive: Bool){
        SKU.text = tempSKU
        prodName.text = tempProdName
        qty.text = tempQty

        if(tempActive == true){
            self.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            self.accessoryType = UITableViewCell.AccessoryType.none
        }
        
    }
    
    
}
