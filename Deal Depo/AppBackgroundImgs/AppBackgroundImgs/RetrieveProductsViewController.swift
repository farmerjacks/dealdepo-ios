//
//  RetrieveProductsViewController.swift
//  AppBackgroundImgs
//
//  Created by Daniel Kearsley on 19/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class RetrieveProductsViewController: UIViewController {
    
    //Products array;
    var globalProducts = [ProductStruct]()
    //Product Struct
    struct ProductStruct {
        var SKU: String = "00000"
        var aisleBay: String = "0000"
        var caseQty: String = "0000"
        var description: String = "0000"
        var gp: String = "0000"
        var metrics: String = "0000"
        var qty: String = "0000"
        var retailPrice: String = "0000"
        var saleDay: String = "0000"
        var supplier: String = "0000"
        var unitPrice: String = "0000"
        var avSales: String = "0000"
        var lastSales: String = "0000"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var test = ProductStruct()
        let test2 = ProductStruct()
        test.SKU = "Hello"
        globalProducts.append(test)
        globalProducts.append(test2)
        self.performSegue(withIdentifier: "ProductSegue", sender: nil)
        //GetProducts(table: "Products", branch: "BUTLER")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destView = segue.destination as! PrductsViewController
        //destView.products = products
    }
    
    func GetProducts(table: String, branch : String){
        let url = URL(string: Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch)
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard data != nil else {
                print("Data is empty")
                return
            }
            
            
            do {
                if let data = data,
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                    let items = json["Items"] as? [[String: Any]] {
                    for item in items {
                        var struc = ProductStruct()
                        if let i = item["SKU"] as? String {
                            struc.SKU = i
                        }
                        if let i = item["aisleBay"] as? String {
                            struc.aisleBay = i
                        }
                        if let i = item["caseQty"] as? String {
                            struc.caseQty = i
                        }
                        if let i = item["description"] as? String {
                            struc.description = i
                        }
                        if let i = item["gp"] as? String {
                            struc.gp = i
                        }
                        if let i = item["metrics"] as? String {
                            struc.metrics = i
                        }
                        if let i = item["qty"] as? String {
                            struc.qty = i
                        }
                        if let i = item["retailPrice"] as? String {
                            struc.retailPrice = i
                        }
                        if let i = item["saleDay"] as? String {
                            struc.saleDay = i
                        }
                        if let i = item["supplier"] as? String {
                            struc.supplier = i
                        }
                        if let i = item["unitPrice"] as? String {
                            struc.unitPrice = i
                        }
                        if let i = item["AvSales"] as? String {
                            struc.avSales = i
                        }
                        if let i = item["lastSales"] as? String {
                            struc.lastSales = i
                        }
                        
                        self.globalProducts.append(struc)
                    }
                }
                
            } catch {
                print("Error deserializing JSON: \(error)")
            }
            self.performSegue(withIdentifier: "ProductSegue", sender: nil)
        }
        task.resume()
    }

}
