//
//  UnstockedProductViewController.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class UnstockedProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var UnstockTableView: UITableView!
    var checkChange = false
    var backSelect = false
    var branch = "BUTLER"
    var supplier = "Deal Depot"
    
    //Unstock struct
    struct UnstockStruct {
        var SKU: String = "0000"
        var description: String = "0000"
        var aisleBay: String = "0000"
        var checked: Bool = false
    }
    
    var tickets = [UnstockStruct]()
    var sendTickets = [UnstockStruct]()
    
    var SKU = [String]()
    
    var productNames = [String]()
    
    var aisleBay = [String]()
    
    var active = [Bool]()
    
    let tickSwitch = UISwitch()

    override func viewDidLoad() {
        super.viewDidLoad()
        SetUserData()
        GetData(table: "UnstockedProducts", branch : branch)
        
        let editButton   = UIBarButtonItem(title: "SAVE",  style: .plain, target: self, action: #selector(didTapDoneButton))
        
        tickSwitch.addTarget(self, action: #selector(TickAllFunc), for: .valueChanged)
        
        let tickButton   = UIBarButtonItem(customView: tickSwitch)
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "BACK", style: .plain, target: self, action: #selector(onClickBack))
        
        self.navigationItem.rightBarButtonItems = [editButton, tickButton]
        
        UnstockTableView.dataSource = self
        UnstockTableView.delegate = self

        
        let nibName = UINib(nibName: "UnstockTabelCell", bundle: nil)
        UnstockTableView.register(nibName, forCellReuseIdentifier: "UnstockCell")
        // Do any additional setup after loading the view.
    }
    
    @objc func didTapDoneButton(sender: AnyObject) {
        let tickets = GetTickets()
        if(tickets != "NULL"){
            SendData(UnstockedProducts: tickets)
        }
    }
    
    @objc func onClickBack(){
        if(checkChange == true){
            CheckMessage()
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func CheckMessage() {
        let alert = UIAlertController(title: "Alert", message: "There are unsaved changes do you wish to save them?", preferredStyle: UIAlertController.Style.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            let tickets = self.GetTickets()
            if(tickets != "NULL"){
                self.backSelect = true
                self.SendData(UnstockedProducts: tickets)
            }
        }
        let noAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func GetTickets() -> String{
        let preferences = UserDefaults.standard
        
        var str = ""
        
        let branch = preferences.string(forKey: "Branch")!
        
        if(sendTickets.count > 0){
            for item in sendTickets{
                str += branch+","
                str += item.SKU+","
                if(item.checked == true){
                    str += "true;"
                } else {
                    str += "false;"
                }
            }
        }
        if(str == ""){
            str = "NULL"
        }
        return str
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            supplier = preferences.string(forKey: "Supplier")!
        }
    }
    
    @objc func TickAllFunc(){
        let len = active.count
        if(len > 0){
            if(tickSwitch.isOn == true){
                for i in 0..<len {
                    active[i] = true
                    tickets[i].checked = true
                    if let location = sendTickets.firstIndex(where: { $0.SKU == tickets[i].SKU }) {
                        sendTickets[location].checked = true
                    } else {
                        sendTickets.append(tickets[i])
                    }
                }
            } else {
                for i in 0..<len {
                    active[i] = false
                    tickets[i].checked = false
                    if let location = sendTickets.firstIndex(where: { $0.SKU == tickets[i].SKU }) {
                        sendTickets[location].checked = false
                    } else {
                        sendTickets.append(tickets[i])
                    }
                }
            }
        }
        checkChange = true
        UnstockTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SKU.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UnstockTableView.dequeueReusableCell(withIdentifier: "UnstockCell", for: indexPath) as! UnstockTabelCell
        cell.commonInit(SKU[indexPath.item], tempProdName: productNames[indexPath.item], tempAisleBay: aisleBay[indexPath.item], tempActive: active[indexPath.item])
    return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView.cellForRow(at: indexPath)?.accessoryType == UITableViewCell.AccessoryType.checkmark){
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.none
            
            active[indexPath.item] = false
            tickets[indexPath.item].checked = false
            
            if let location = sendTickets.firstIndex(where: { $0.SKU == tickets[indexPath.item].SKU }) {
                sendTickets[location].checked = false
            } else {
                sendTickets.append(tickets[indexPath.item])
            }
        } else {
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
            
            active[indexPath.item] = true
            tickets[indexPath.item].checked = true
            
            if let location = sendTickets.firstIndex(where: { $0.SKU == tickets[indexPath.item].SKU }) {
                sendTickets[location].checked = true
            } else {
                sendTickets.append(tickets[indexPath.item])
            }
        }
        checkChange = true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func SetData(){
        SKU = [String]()
        productNames = [String]()
        aisleBay = [String]()
        active = [Bool]()
        for item in tickets{
            SKU.append(item.SKU)
            productNames.append(item.description)
            aisleBay.append(item.aisleBay)
            active.append(item.checked)
        }
        UnstockTableView.reloadData()
    }
    
    
    func GetData(table: String, branch : String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator(title: Constants.ERRORS.LOADING_TICKETS)
            let urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch+"&"+Constants.URLS.APIKEY
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                if(data == nil){
                    self.progress.hide()
                }
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = UnstockStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["aisleBay"] as? String {
                                struc.aisleBay = i
                            }
                            if let i = item["flag"] as? String {
                                if(i == "false"){
                                    struc.checked = false
                                } else {
                                    struc.checked = true
                                }
                            }
                            if let i = item["description"] as? String {
                                struc.description = i
                            }
                            
                            self.tickets.append(struc)
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.SortProducts()
                            self.SetData()
                            self.progress.hide()
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    //Activity Indiciator
    var progress = ProgressHUD(text: "Saving")
    
    func SendData(UnstockedProducts: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator(title: "Saving")
            let json: [String: Any] = ["values": UnstockedProducts,
                                       "table": Constants.URLS.TABLE_UNSTOCKED_PRODUCTS,
                                       "apiKey": Constants.URLS.APIKEY2]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/write");
            
            var request = URLRequest(url:myUrl!)
            
            request.setValue("text/json", forHTTPHeaderField: "Content-Type")
            
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil
                {
                    //print("error=\(error)")
                    return
                }
                
                // You can print out response object
                //print("response = \(response)")
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                        self.progress.hide()
                    }
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        if(items["Message"] != nil){
                            msg = items["Message"] as! String
                        } else {
                            msg = "Error!"
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.sendTickets = []
                            self.checkChange = false
                            if(self.backSelect == true){
                                self.MessageOther(msg: msg)
                            } else {
                                self.Message(msg: msg)
                            }
                            self.progress.hide()
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func MessageOther(msg: String){
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        let noAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func activityIndicator(title: String) {
        progress = ProgressHUD(text: title)
        self.view.addSubview(self.progress)
        progress.show()
    }

    //Sort Products by ailse and bay then alpha
    func SortProducts(){
        self.tickets = self.tickets.sorted(by: { $0.aisleBay < $1.aisleBay })
        var current = ""
        var prod = [UnstockStruct()]
        prod = []
        var array = [UnstockStruct()]
        array = []
        for item in self.tickets{
            if(current == ""){
                current = item.aisleBay
            }
            
            if(current != item.aisleBay){
                current = item.aisleBay
                array = array.sorted(by: { $0.description < $1.description })
                prod.append(contentsOf: array)
                array = []
            }
            
            array.append(item)
        }
        prod.append(contentsOf: array)
        
        self.tickets = prod
    }
}
