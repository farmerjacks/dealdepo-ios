//
//  FoundProductViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 26/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit
import ALCameraViewController

class FoundProductViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    //Image Picker
    var imagePicker: UIImagePickerController!
    var product = [PrductsViewController.ProductStruct()]
    var index = 0
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var sku: UILabel!
    @IBOutlet weak var supplier: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var yest: UILabel!
    @IBOutlet weak var avg: UILabel!
    @IBOutlet weak var last: UILabel!
    @IBOutlet weak var norm: UILabel!
    @IBOutlet weak var caseQty: UILabel!
    @IBOutlet weak var gp: UILabel!
    @IBOutlet weak var ab: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let preferences = UserDefaults.standard
        var priv = "employee"
        if(preferences.object(forKey: "privilege") != nil) {
            priv = (preferences.string(forKey: "privilege") as String?)!
        }
        
        if(priv == "admin"){
            let editButton   = UIBarButtonItem(title: "TAKE PHOTO",  style: .plain, target: self, action: #selector(didTapDoneButton))
            
            self.navigationItem.rightBarButtonItem = editButton
        }

        SetData()
        // Do any additional setup after loading the view.
    }
    
    func SetData(){
        name.text = product[index].description
        sku.text = product[index].SKU
        supplier.text = product[index].supplier
        unit.text = product[index].unitPrice
        yest.text = product[index].qty.components(separatedBy: ".")[0]
        avg.text = product[index].avSales
        last.text = product[index].lastSales
        norm.text = product[index].retailPrice
        caseQty.text = product[index].caseQty
        gp.text = product[index].gp
        ab.text = product[index].aisleBay
        
        downloadImage(url: URL(string: "https://s3.amazonaws.com/imagesFarmerJacks/"+product[index].SKU)!)
    }
    
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                if let image = UIImage(data: data){
                    self.productImage.image = image
                }
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        if(index > 0){
            index -= 1
            SetData()
        }
    }
    
    @IBAction func next(_ sender: Any) {
        if(index < product.count-1){
            index += 1
            SetData()
        }
    }
    
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Take Photo
    @objc func didTapDoneButton(){
        /*if !UIImagePickerController.isSourceTypeAvailable(.camera){
            
            let alertController = UIAlertController.init(title: nil, message: "Device has no camera.", preferredStyle: .alert)
            
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
            })
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }*/
        
        let croppingEnabled = true
        let cameraViewController = CameraViewController() { [weak self] image, asset in
            // Do something with your image here.
            // If cropping is enabled this image will be the cropped version
            
            self?.productImage.image = image
            
            if let imageData = image!.jpegData(compressionQuality: 0.2) {
                let data =  Data(imageData)
                let strBase64:String = imageData.base64EncodedString(options: [.lineLength64Characters])
                let str = strBase64
                if(str != ""){
                    self?.SendData(str: str)
                }
            } else {
                print("can't get JPEG representation")
            }
            
            self?.dismiss(animated: true, completion: nil)
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        imagePicker.dismiss(animated: true, completion: nil)
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
            as! UIImage
        
        productImage.image = image
        
            if let imageData = image.jpegData(compressionQuality: 0.9) {
                let data =  Data(imageData)
                let strBase64:String = imageData.base64EncodedString(options: [.lineLength64Characters])
                let str = strBase64
                if(str != ""){
                    SendData(str: str)
                }
            } else {
                print("can't get JPEG representation")
            }
    }
    
    var progress = ProgressHUD(text: "Sending Image")
    
    func SendData(str: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let json: [String: Any] = ["values": str,
                                       "imageId": self.product[index].SKU,
                                       "table": Constants.URLS.TABLE_IMAGES,
                                       "apiKey": Constants.URLS.APIKEY2]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/write");
            
            var request = URLRequest(url:myUrl!)
            
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                self.progress.show()
                if error != nil
                {
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        self.progress.hide()
                        if(items["Message"] != nil){
                            msg = items["Message"] as! String
                        } else {
                            msg = "Error!"
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            self.Message(msg: msg)
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
        
    }
    
    func activityIndicator() {
        self.view.addSubview(self.progress)
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
