//
//  OrderHistoryViewController.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class OrderHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var OrderHistoryTableView: UITableView!
    
    var weekday = ["SU", "M", "TU", "W", "TH", "F", "SA"]
    var weekDayName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    var branch = "BUTLER"
    var supplier = "Deal Depot"
    
    //Print Ticket struct
    struct OrderStruct {
        var SKU: String = "0000"
        var description: String = "0000"
        var date: String = "00/00/0000"
        var qty: String = "0000"
        var aisleBay: String = "00-00"
        var orderDay: String = ""
    }
    
    var orderData = [OrderStruct]()
    
    var orders = [String]()
    var selectOrder = [OrderStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUserData()
        GetData(table: "Orders", branch : branch)
        
        OrderHistoryTableView.dataSource = self
        OrderHistoryTableView.delegate = self
        
        
        let nibName = UINib(nibName: "OrderHistoryTableCell", bundle: nil)
        OrderHistoryTableView.register(nibName, forCellReuseIdentifier: "OrderCell")
        // Do any additional setup after loading the view.
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            supplier = preferences.string(forKey: "Supplier")!
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let orderController = segue.destination as! OldOrderReviewViewController
        orderController.orderData = selectOrder
        orderController.orderDay = weekday.firstIndex(of: selectOrder[0].orderDay)!
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrderHistoryTableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderHistoryTableCell
        cell.commonInit(orders[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectOrder = FindOrders(date: orders[indexPath.item])
        if(!selectOrder.isEmpty){
            performSegue(withIdentifier: "segue", sender: self)
        }
        
        //self.navigationController?.pushViewController(OldOrderReviewViewController(), animated: true)
        //self.OrderHistoryTableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func FindOrders(date: String) ->[OrderStruct]{
        var orders = [OrderStruct]()
        
        for item in orderData{
            if(DefineDay(day: item.orderDay) == date){
                orders.append(item)
            }
        }
        
        return orders
    }
    

    func SetData(){
        orders = [String]()
        for item in orderData{
            if(!orders.contains(DefineDay(day: item.orderDay))){
                orders.append(DefineDay(day: item.orderDay))
            }
        }
        OrderHistoryTableView.reloadData()
    }
    
    func DefineDay(day: String) -> String{
        var week = ""
        if let index = weekday.firstIndex(of: day){
            var next = index+1
            if(index == 6){
                next = 0
            }
            week = weekDayName[index]+" => "+weekDayName[next]
        }
        return week
    }
    
    var progress = ProgressHUD(text: "Loading Orders")
    func GetData(table: String, branch : String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch+"&"+Constants.URLS.APIKEY
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                if(data == nil){
                    self.progress.hide()
                }
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                        self.progress.hide()
                    }
                }
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = OrderStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["description"] as? String {
                                struc.description = i
                            }
                            if let i = item["date"] as? String {
                                struc.date = i
                            }
                            if let i = item["qty"] as? String {
                                struc.qty = i
                            }
                            if let i = item["orderDay"] as? String {
                                struc.orderDay = i
                            }
                            if let i = item["aisleBay"] as? String {
                                struc.aisleBay = i
                            }
                            
                            self.orderData.append(struc)
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.SetData()
                            self.progress.hide()
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func activityIndicator() {
        self.view.addSubview(self.progress)
        progress.show()
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
