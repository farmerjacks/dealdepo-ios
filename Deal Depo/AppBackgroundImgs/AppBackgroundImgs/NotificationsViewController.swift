//
//  NotificationsViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 27/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate {
    //Data
    var currentIndex = 0
    var progress = ProgressHUD(text: "Updating")
    var subjectArray = [String]()
    var messageArray = [String]()
    var intervalArray = [String]()
    var weekDayName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    //Components
    @IBOutlet weak var dayTable: UITableView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var submit: UIButton!
    
    @IBOutlet weak var monday: UISwitch!
    @IBOutlet weak var tuesday: UISwitch!
    @IBOutlet weak var wednsday: UISwitch!
    @IBOutlet weak var thursday: UISwitch!
    @IBOutlet weak var friday: UISwitch!
    @IBOutlet weak var saturday: UISwitch!
    @IBOutlet weak var sunday: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Tap event on picker view
        let pickerTap = UITapGestureRecognizer(target: self, action: #selector(self.pickerTapped(tapRecognizer:)))
        pickerTap.cancelsTouchesInView = false
        pickerTap.delegate = self
        picker.addGestureRecognizer(pickerTap)
        
        //Set the right bar button to trigger the show list function
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "LIST", style: .plain, target: self, action: #selector(ShowList))

        //Set the textview border
        message.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        message.layer.borderWidth = 1.0;
        message.layer.cornerRadius = 5.0;
        
        //Get data from the notification table
        GetData(table: "Notifications")
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NotificationsViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    //Tap event on pickerview
    @objc func pickerTapped(tapRecognizer:UITapGestureRecognizer) {
        if (tapRecognizer.state == UIGestureRecognizer.State.ended) {
            let rowHeight : CGFloat  = self.picker.rowSize(forComponent: 0).height
            let selectedRowFrame: CGRect = self.picker.bounds.insetBy(dx: 0.0, dy: (self.picker.frame.height - rowHeight) / 2.0 )
            let userTappedOnSelectedRow = (selectedRowFrame.contains(tapRecognizer.location(in: picker)))
            if (userTappedOnSelectedRow) {
                let row = self.picker.selectedRow(inComponent: 0)
                currentIndex = row
                subject.text = subjectArray[currentIndex]
                message.text = messageArray[currentIndex]
                SetSwitchs(array: intervalArray[currentIndex])
                picker.isHidden = true
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    //Shows the picker
    @objc func ShowList(){
        if(picker.isHidden == true){
            picker.isHidden = false
        } else {
            picker.isHidden = true
        }
    }
    
    //Sets the values to default for creating a new notification
    @IBAction func newButton(_ sender: Any) {
        message.text = ""
        subject.text = ""
        monday.isOn = false
        tuesday.isOn = false
        wednsday.isOn = false
        thursday.isOn = false
        friday.isOn = false
        saturday.isOn = false
        sunday.isOn = false
    }
    
    //Trigger the delete
    @IBAction func DeleteButton(_ sender: Any) {
        Delete()
    }
    
    //Delete a notification from the database
    func Delete(){
        if(subjectArray.count > 0){
            let msg = subjectArray[currentIndex]
            subjectArray.remove(at: currentIndex)
            messageArray.remove(at: currentIndex)
            intervalArray.remove(at: currentIndex)
            picker.reloadAllComponents()
            SendData(message: msg, table: "NotificationsDelete")
            currentIndex = 0
            if(subjectArray.count > 0){
                self.subject.text = self.subjectArray[currentIndex]
                self.message.text = self.messageArray[currentIndex]
                self.SetSwitchs(array: self.intervalArray[currentIndex])
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Submit action to add a new notification
    @IBAction func submitAction(_ sender: Any) {
        var msg = ""
        let sub = subject.text!
        let notify = message.text!
        let intervals = GetSwitchs()
        
        if(sub != "" && notify != ""){
            msg = sub+","+notify+intervals
            SendData(message: msg, table: "Notifications")
            subjectArray.append(sub)
            messageArray.append(notify)
            intervalArray.append(intervals)
            self.picker.reloadAllComponents()
        } else {
            Message(msg: "Please fill out all fields!")
        }
    }
    
    //Get all the switches and create a string of all the days
    func GetSwitchs() -> String{
        var str = ""
        if(sunday.isOn){
            str += ",true"
        } else {
            str += ",false"
        }
        
        if(monday.isOn){
            str += "-true"
        } else {
            str += "-false"
        }
        
        if(tuesday.isOn){
            str += "-true"
        } else {
            str += "-false"
        }
        
        if(wednsday.isOn){
            str += "-true"
        } else {
            str += "-false"
        }
        
        if(thursday.isOn){
            str += "-true"
        } else {
            str += "-false"
        }
        
        if(friday.isOn){
            str += "-true"
        } else {
            str += "-false"
        }
        
        if(saturday.isOn){
            str += "-true"
        } else {
            str += "-false"
        }
        
        return str
    }
    
    //Get all the notifications from the DBAccess api
    func GetData(table: String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+"&"+Constants.URLS.APIKEY
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    self.progress.hide()
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                        self.progress.hide()
                    }
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            if let i = item["Subject"] as? String {
                                self.subjectArray.append(i)
                            }
                            if let i = item["Message"] as? String {
                                self.messageArray.append(i)
                            }
                            if let i = item["Interval"] as? String {
                                self.intervalArray.append(i)
                            }
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            if(self.subjectArray.count > 0){
                                self.subject.text = self.subjectArray[0]
                                self.message.text = self.messageArray[0]
                                self.SetSwitchs(array: self.intervalArray[0])
                                self.picker.reloadAllComponents()
                            }
                            self.progress.hide()
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    //Send the data
    func SendData(message: String, table: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            self.progress = ProgressHUD(text: "Updating")
            self.view.addSubview(self.progress)
            self.progress.show()
            
            let json: [String: Any] = ["values": message,
                                       "table": table,
                                       "apiKey": Constants.URLS.APIKEY2]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)

            let myUrl = URL(string: Constants.URLS.BASE+"/write");
            
            var request = URLRequest(url:myUrl!)
            
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil
                {
                    return
                }
                
                if(data == nil){
                    self.progress.hide()
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        
                        if(items["Message"] != nil){
                            msg = items["Message"] as! String
                        } else {
                            msg = "Error!"
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            self.Message(msg: msg)
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjectArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjectArray.count
    }
    
    /*@objc(pickerView:didSelectRow:inComponent:) func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentIndex = row
        subject.text = subjectArray[currentIndex]
        message.text = messageArray[currentIndex]
        SetSwitchs(array: intervalArray[currentIndex])
        picker.isHidden = true
    }*/
    
    //Set the switches
    func SetSwitchs(array: String){
        var arr = array.components(separatedBy: "-")
        if(arr[0] == "true"){
            sunday.isOn = true
        } else {
            sunday.isOn = false
        }
        
        if(arr[1] == "true"){
            monday.isOn = true
        } else {
            monday.isOn = false
        }
        
        if(arr[2] == "true"){
            tuesday.isOn = true
        } else {
            tuesday.isOn = false
        }
        
        if(arr[3] == "true"){
            wednsday.isOn = true
        } else {
            wednsday.isOn = false
        }
        
        if(arr[4] == "true"){
            thursday.isOn = true
        } else {
            thursday.isOn = false
        }
        
        if(arr[5] == "true"){
            friday.isOn = true
        } else {
            friday.isOn = false
        }
        
        if(arr[6] == "true"){
            saturday.isOn = true
        } else {
            saturday.isOn = false
        }
    }
    
    //Show the message alert
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //Show the loading
    func activityIndicator() {
        progress = ProgressHUD(text: Constants.ERRORS.LOADING_NOTIFICATIONS)
        self.view.addSubview(self.progress)
        progress.show()
    }
}
