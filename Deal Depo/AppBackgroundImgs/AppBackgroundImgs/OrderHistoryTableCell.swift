//
//  OrderHistoryTableCell.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class OrderHistoryTableCell: UITableViewCell {

    @IBOutlet weak var curOrder: UILabel! 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(_ tempOrder: String){
 
        curOrder.text = tempOrder
    }
    
}
