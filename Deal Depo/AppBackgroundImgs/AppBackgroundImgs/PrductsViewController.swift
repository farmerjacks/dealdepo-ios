//
//  PrductsViewController.swift
//  AppBackgroundImgs
//
//  Created by Daniel Kearsley on 18/7/17.
//  Copyright © 2017 SCS, DanCom Software All rights reserved.
//

import UIKit

class PrductsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate {
    
    //Box Views
    @IBOutlet weak var borderLeft: UIView!
    @IBOutlet weak var borderRight: UIView!
    @IBOutlet var pickerCarrier: UIView!
    
    var saved = true
    var branch = "BUTLER"
    var selSupplier = "Deal Depot"
    var storeCode = "BUT"
    
    var weekday = ["SU", "M", "TU", "W", "TH", "F", "SA"]
    var weekDayName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    var aislBayIndex = 0
    
    ///Products array;
    var products = [ProductStruct]()
    //Product Struct
    struct ProductStruct {
        var SKU: String = "N/A"
        var aisleBay: String = "N/A"
        var caseQty: String = "N/A"
        var description: String = "N/A"
        var gp: String = "N/A"
        var metrics: String = "N/A"
        var qty: String = "N/A"
        var retailPrice: String = "N/A"
        var saleDay: String = "N/A"
        var supplier: String = "N/A"
        var unitPrice: String = "N/A"
        var avSales: String = "N/A"
        var lastSales: String = "N/A"
        var getQty: String = "0"
        var abUpdate: Bool = false
        var ChangeAisle = "0"
        var ChangeBay = "0"
        var tickReq: Bool = false
        var tickReqNum: String = "0"
        var image: UIImage? = nil
        var transitStock = "N/A"
    }
    
    @IBOutlet weak var orderDayLabel: UILabel!
    @IBOutlet weak var aisleBayButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var reviewButton: UIButton!
    //Current Product index
    var currentIndex = 0
    
    //Activity Indiciator
    var progress = ProgressHUD(text: "Loading Products")
    
    //Product Labels
    @IBOutlet weak var SKU: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var supplier: UILabel!
    @IBOutlet weak var yestSale: UILabel!
    @IBOutlet weak var avSales: UILabel!
    @IBOutlet weak var lastWeekSales: UILabel!
    @IBOutlet weak var unitCost: UILabel!
    @IBOutlet weak var normalSell: UILabel!
    @IBOutlet weak var caseQty: UILabel!
    @IBOutlet weak var currentGP: UILabel!
    @IBOutlet weak var aisle: UIButton!
    @IBOutlet weak var transitStock: UILabel!
    
    //Update tick values
    @IBOutlet weak var abUVal: UISwitch!
    @IBOutlet weak var tickReqVal: UISwitch!
    
    //Aisle and Bay arrays
    var aisleBayArray = [String]()
    //Picker view
    @IBOutlet weak var picker: UIPickerView!
    
    //Aisle and bay update
    @IBOutlet weak var aisleBayView: UIView!
    @IBOutlet weak var bayTextField: UITextField!
    @IBOutlet weak var aisleTextField: UITextField!
    
    //Ticket Required
    @IBOutlet weak var TickReqNum: UITextField!
    @IBOutlet weak var TicketReqView: UIView!
    
    var changeDay = false
    
    var orderDay = ""
    
    @IBAction func TickReqNumAction(_ sender: Any) {
        saved = false
        if(TickReqNum.text!.isEmpty){
            TickReqNum.text = "1"
            products[currentIndex].tickReqNum = "1"
            DispatchQueue.main.async {
                self.TickReqNum.selectAll(nil)
            }
        } else {
            products[currentIndex].tickReqNum = TickReqNum.text!
        }
    }
    //Tick req num change value
    @IBAction func TickReqValueChange(_ sender: Any) {
        saved = false
        if(TickReqNum.text!.isEmpty){
            TickReqNum.text = "1"
            products[currentIndex].tickReqNum = "1"
            DispatchQueue.main.async {
                self.TickReqNum.selectAll(nil)
            }
        } else {
            products[currentIndex].tickReqNum = TickReqNum.text!
        }
    }
    
    @IBAction func tickReqEditChange(_ sender: Any) {
        saved = false
        if(TickReqNum.text!.isEmpty){
            TickReqNum.text = "1"
            products[currentIndex].tickReqNum = "1"
            DispatchQueue.main.async {
                self.TickReqNum.selectAll(nil)
            }
        } else {
            products[currentIndex].tickReqNum = TickReqNum.text!
        }
    }
    //Save
    @IBAction func SaveOrder(_ sender: Any) {
        if(UserDefaults.standard.bool(forKey: "Modify") == true){
            CheckModifyMessage()
        } else {
            Save()
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(changeDay == false){
            return aisleBayArray[row]
        } else {
            return weekDayName[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(changeDay == false){
            return aisleBayArray.count
        } else {
            return weekDayName.count
        }
    }
    
    /*@objc(pickerView:didSelectRow:inComponent:) func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        saved = false
        if(changeDay == false){
            aislBayIndex = row
            let i = products.index { $0.aisleBay == aisleBayArray[row] }
            currentIndex = i!;
            self.SetData()
            picker.isHidden = true
        } else {
            orderDay = weekday[row]
            print(orderDay)
            
            var next = row+1
            if(row == 6){
                next = 0
            }
            self.orderDayLabel.text = weekDayName[row]+" => "+weekDayName[next]
            self.RefreshOrder()
            if(UserDefaults.standard.object(forKey: "Products") != nil){
                if let data = UserDefaults.standard.object(forKey: "Products") as? [[String]] {
                    self.SetProducts(array: data)
                    self.SetData()
                }
            }
            picker.isHidden = true
        }
        self.view.endEditing(true)
        
        //self.picker.isHidden = true
    }*/
    
    func SaveProducts()  throws -> [[String]]{
        var array = [[String]]()
        for item in products{
            let qty = Int(item.getQty)
            if(qty != nil){
                if(qty! > 0){
                    var prod = [String]()
                    if(!item.SKU.isEmpty){
                        prod.append(item.SKU)
                    } else {
                        prod.append("N/A")
                    }
                    prod.append(item.getQty)
                    if(!item.tickReqNum.isEmpty){
                        prod.append(item.tickReqNum)
                    } else {
                        prod.append("N/A")
                    }
                    if(item.tickReq == true){
                        prod.append("true")
                    } else {
                        prod.append("false")
                    }
                    if(!item.ChangeAisle.isEmpty){
                        prod.append(item.ChangeAisle)
                    } else {
                        prod.append("N/A")
                    }
                    if(!item.ChangeBay.isEmpty){
                        prod.append(item.ChangeBay)
                    } else {
                        prod.append("N/A")
                    }
                    if(item.abUpdate == true){
                        prod.append("true")
                    } else {
                        prod.append("false")
                    }
                    prod.append(orderDay)
                    
                    array.append(prod)
                }
            }
        }
        
        return array
    }
    
    func SetProducts(array: [[String]]) {
        for item in array{
            if let location = products.firstIndex(where: { $0.SKU == item[0] }) {
                let day = weekday.firstIndex(of: orderDay)
                if(CheckForNewDay(orderDay: day!)){
                    if(item[7] == orderDay){
                        products[location].getQty = item[1]
                        products[location].tickReqNum = item[2]
                        if(item[3] == "true"){
                            products[location].tickReq = true
                        } else {
                            products[location].tickReq = false
                        }
                        products[location].ChangeAisle = item[4]
                        products[location].ChangeBay = item[5]
                        if(item[6] == "true"){
                            products[location].abUpdate = true
                        } else {
                            products[location].abUpdate = false
                        }
                    }
                }
            }
        }
    }
    
    func CheckForNewDay(orderDay: Int) -> Bool{
        let date = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.weekday, from: date)
        var index = day-1
        if(index == 6){
            index = 0
        }
        
        if(index < orderDay || index > orderDay){
            return false
        } else {
            return true
        }
    }
    
    //Update Tick boxes actions
    @IBAction func abUpTick(_ sender: Any) {
        saved = false
        products[currentIndex].abUpdate = abUVal.isOn
        if(aisleBayView.isHidden == false){
            aisleBayView.isHidden = true
        } else {
            aisleBayView.isHidden = false
        }
    }
    
    
    @IBAction func AddAisle(_ sender: Any) {
        saved = false
        
        if(aisleTextField.text!.isEmpty){
            aisleTextField.text = "0"
            products[currentIndex].ChangeAisle = "0"
            DispatchQueue.main.async {
                self.aisleTextField.selectAll(nil)
            }
        } else {
            products[currentIndex].ChangeAisle = aisleTextField.text!
        }
    }
    //Aisle value changed
    @IBAction func AisleChange(_ sender: Any) {
        saved = false
        if(aisleTextField.text!.isEmpty){
            aisleTextField.text = "0"
            products[currentIndex].ChangeAisle = "0"
            DispatchQueue.main.async {
                self.aisleTextField.selectAll(nil)
            }
        } else {
            products[currentIndex].ChangeAisle = aisleTextField.text!
        }
    }
    
    @IBAction func aisleEditChange(_ sender: Any) {
        saved = false
        if(aisleTextField.text!.isEmpty){
            aisleTextField.text = "0"
            products[currentIndex].ChangeAisle = "0"
            DispatchQueue.main.async {
                self.aisleTextField.selectAll(nil)
            }
        } else {
            products[currentIndex].ChangeAisle = aisleTextField.text!
        }
    }
    
    @IBAction func AddBay(_ sender: Any) {
        saved = false
        if(bayTextField.text!.isEmpty){
            bayTextField.text = "0"
            products[currentIndex].ChangeBay = "0"
            DispatchQueue.main.async {
                self.bayTextField.selectAll(nil)
            }
        } else {
            products[currentIndex].ChangeBay = bayTextField.text!
        }
    }
    //Bay change value
    @IBAction func BayChange(_ sender: Any) {
        saved = false
        if(bayTextField.text!.isEmpty){
            bayTextField.text = "0"
            products[currentIndex].ChangeBay = "0"
            DispatchQueue.main.async {
                self.bayTextField.selectAll(nil)
            }
        } else {
            products[currentIndex].ChangeBay = bayTextField.text!
        }
    }
    
    @IBAction func BayEditChange(_ sender: Any) {
        saved = false
        if(bayTextField.text!.isEmpty){
            bayTextField.text = "0"
            products[currentIndex].ChangeBay = "0"
            DispatchQueue.main.async {
                self.bayTextField.selectAll(nil)
            }
        } else {
            products[currentIndex].ChangeBay = bayTextField.text!
        }
    }
    
    
    @IBAction func EnterBay(_ sender: Any) {
        DispatchQueue.main.async {
            self.bayTextField.selectAll(nil)
        }
    }
    
    @IBAction func EnterAisle(_ sender: Any) {
        DispatchQueue.main.async {
            self.aisleTextField.selectAll(nil)
        }
    }
    
    //Ticket required
    
    @IBAction func tickReq(_ sender: Any) {
        saved = false
        products[currentIndex].tickReq = tickReqVal.isOn
        if(tickReqVal.isOn == true){
            products[currentIndex].tickReqNum = TickReqNum.text!
            TicketReqView.isHidden = false
        } else {
            products[currentIndex].tickReqNum = "0"
            TicketReqView.isHidden = true
        }
    }
    
    @IBAction func EnterTicketReq(_ sender: Any) {
        DispatchQueue.main.async {
            self.TickReqNum.selectAll(nil)
        }
    }
    @IBAction func next(_ sender: Any) {
        if(currentIndex < products.count-1){
            saved = false
            currentIndex += 1
            SetData()
            DispatchQueue.main.async {
                self.qtyText.selectAll(nil)
                self.qtyText.becomeFirstResponder()
            }
        }
    }
    @IBAction func back(_ sender: Any) {
        if(currentIndex > 0){
            saved = false
            currentIndex -= 1
            SetData()
            DispatchQueue.main.async {
                self.qtyText.selectAll(nil)
                self.qtyText.becomeFirstResponder()
            }
        }
    }
    
    //Change Aisle and bay
    @IBAction func ChangeAisle(_ sender: Any) {
        if(pickerCarrier.isHidden == true){
            changeDay = false
            SetAisleBayData()
            picker.reloadAllComponents()
            pickerCarrier.isHidden = false
            if let aIndex = aisleBayArray.firstIndex(of: products[currentIndex].aisleBay) {
                aislBayIndex = aIndex
            } else {
                aislBayIndex = 0
            }
            picker.selectRow(aislBayIndex, inComponent: 0, animated: true)
        } else {
            pickerCarrier.isHidden = true
        }
    }
    
    //Quantity Text field
    @IBOutlet weak var qtyText: UITextField!
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        saved = false
        if(qtyText.text!.isEmpty){
            qtyText.text = "0"
            products[currentIndex].getQty = "0"
            DispatchQueue.main.async {
                self.qtyText.selectAll(nil)
            }
        } else {
            products[currentIndex].getQty = qtyText.text!
        }
    }
    //Qty value changed
    @IBAction func QtyChanged(_ sender: Any) {
        saved = false
        if(qtyText.text!.isEmpty){
            qtyText.text = "0"
            products[currentIndex].getQty = "0"
            DispatchQueue.main.async {
                self.qtyText.selectAll(nil)
            }
        } else {
            products[currentIndex].getQty = qtyText.text!
        }
    }
    
    @IBAction func qtyEditChange(_ sender: Any) {
        saved = false
        if(qtyText.text!.isEmpty){
            qtyText.text = "0"
            products[currentIndex].getQty = "0"
            DispatchQueue.main.async {
                self.qtyText.selectAll(nil)
            }
        } else {
            products[currentIndex].getQty = qtyText.text!
        }
    }
    @IBAction func EnterQtyText(_ sender: Any) {
        DispatchQueue.main.async {
            self.qtyText.selectAll(nil)
        }
    }
    
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Tap event on picker view
        let pickerTap = UITapGestureRecognizer(target: self, action: #selector(self.pickerTapped(tapRecognizer:)))
        pickerTap.cancelsTouchesInView = false
        pickerTap.delegate = self
        picker.addGestureRecognizer(pickerTap)
        
        //Borders
        borderLeft.layer.borderWidth = 2
        borderLeft.layer.borderColor = UIColor.black.cgColor
        borderRight.layer.borderWidth = 2
        borderRight.layer.borderColor = UIColor.black.cgColor
        
        mainView.layer.borderWidth = 2
        mainView.layer.borderColor = UIColor.black.cgColor
        
        UserDefaults.standard.set(false, forKey: "OrderSent")
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "BACK", style: .plain, target: self, action: #selector(onClickBack))
        
        let date = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.weekday, from: date)
        let index = day-1
        
        orderDay = weekday[index]
        var next = index+1
        if(index == 6){
            next = 0
        }
        orderDayLabel.text = weekDayName[index]+" => "+weekDayName[next]
        
        let editButton   = UIBarButtonItem(title: "CHANGE ORDER DAY",  style: .plain, target: self, action: #selector(didTapDoneButton))
        
        self.navigationItem.rightBarButtonItem = editButton
        
        //Change to number pad
        aisleTextField.keyboardType = UIKeyboardType.numberPad
        bayTextField.keyboardType = UIKeyboardType.numberPad
        qtyText.keyboardType = UIKeyboardType.numberPad
        TickReqNum.keyboardType = UIKeyboardType.numberPad
        
        pickerCarrier.isHidden = true
        //Add text change listener
        qtyText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        SetUserData()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PrductsViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        SetAisleBayData()
        
        view.addGestureRecognizer(tap)
        
        // ToolBar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: picker.frame.width, height: 44))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(PrductsViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(PrductsViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        picker.addSubview(toolBar)
    }
    
    //MARK:- Button
    @objc func doneClick() {
        pickerCarrier.isHidden = true
        
        let row = self.picker.selectedRow(inComponent: 0)
        saved = false
        if(changeDay == false){
            aislBayIndex = row
            let i = products.firstIndex { $0.aisleBay == aisleBayArray[row] }
            currentIndex = i!;
            self.SetData()
            pickerCarrier.isHidden = true
        } else {
            orderDay = weekday[row]
            var next = row+1
            if(row == 6){
                next = 0
            }
            self.orderDayLabel.text = weekDayName[row]+" => "+weekDayName[next]
            self.RefreshOrder()
            if(UserDefaults.standard.object(forKey: "Products") != nil){
                if let data = UserDefaults.standard.object(forKey: "Products") as? [[String]] {
                    self.SetProducts(array: data)
                    self.SetData()
                }
            }
            pickerCarrier.isHidden = true
        }
    }
    @objc func cancelClick() {
        pickerCarrier.isHidden = true
    }
    
    @objc func pickerTapped(tapRecognizer:UITapGestureRecognizer) {
        if (tapRecognizer.state == UIGestureRecognizer.State.ended) {
            let rowHeight : CGFloat  = self.picker.rowSize(forComponent: 0).height
            let selectedRowFrame: CGRect = self.picker.bounds.insetBy(dx: 0.0, dy: (self.picker.frame.height - rowHeight) / 2.0 )
            let userTappedOnSelectedRow = (selectedRowFrame.contains(tapRecognizer.location(in: picker)))
            if (userTappedOnSelectedRow) {
                let row = self.picker.selectedRow(inComponent: 0)
                saved = false
                if(changeDay == false){
                    aislBayIndex = row
                    let i = products.firstIndex { $0.aisleBay == aisleBayArray[row] }
                    currentIndex = i!;
                    self.SetData()
                    pickerCarrier.isHidden = true
                } else {
                    orderDay = weekday[row]
                    var next = row+1
                    if(row == 6){
                        next = 0
                    }
                    self.orderDayLabel.text = weekDayName[row]+" => "+weekDayName[next]
                    self.RefreshOrder()
                    if(UserDefaults.standard.object(forKey: "Products") != nil){
                        if let data = UserDefaults.standard.object(forKey: "Products") as? [[String]] {
                            self.SetProducts(array: data)
                            self.SetData()
                        }
                    }
                    pickerCarrier.isHidden = true
                }
                self.view.endEditing(true)
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.qtyText.selectAll(nil)
            self.qtyText.becomeFirstResponder()
        }
        if let savedSKU = UserDefaults.standard.string(forKey: "savedIndex"){
            if let location = products.firstIndex(where: { $0.SKU == savedSKU }) {
                currentIndex = location
                self.SetData()
                UserDefaults.standard.removeObject(forKey: "savedIndex")
            }
        }
    }
    
    @objc func onClickBack(){
        if(saved == false){
            CheckMessage()
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @objc func didTapDoneButton(sender: AnyObject) {
        if(pickerCarrier.isHidden == true){
            changeDay = true
            let index = weekday.firstIndex(of: orderDay)
            picker.reloadAllComponents()
            pickerCarrier.isHidden = false
            picker.selectRow(index!, inComponent: 0, animated: true)
        } else {
            pickerCarrier.isHidden = true
        }
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func ReviewOrderAction(_ sender: Any) {
        DispatchQueue.main.async() { () -> Void in
            self.performSegue(withIdentifier: "ReviewOrder", sender: self)
        }
    }
    
    func CheckForNull(){
        let len = products.count
        
        for i in 0..<len{
            if(products[i].getQty == "NULL"){
                products[i].getQty = "0"
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ReviewOrder"){
            CheckForNull()
            let destView = segue.destination as! ReviewOrderViewController
            destView.products = products
            destView.orderDay = orderDay
            destView.saved = saved
        }
    }
    
    func SetAisleBayData(){
        aisleBayArray = [String]()
        var temp = ""
        for item in products {
            if(temp == "" || temp != item.aisleBay){
                if(item.aisleBay != "NULL"){
                    temp = item.aisleBay
                    aisleBayArray.append(item.aisleBay)
                }
            }
        }
        
        picker.reloadAllComponents()
    }
    
    @IBOutlet weak var productImage: UIImageView!
    func SetData(){
        if(currentIndex < products.count){
            self.SKU.text = products[currentIndex].SKU
            self.supplier.text = products[currentIndex].supplier
            self.name.text = products[currentIndex].description
            self.lastWeekSales.text = products[currentIndex].lastSales
            self.avSales.text = products[currentIndex].avSales
            self.yestSale.text = products[currentIndex].qty.components(separatedBy: ".")[0]
            self.unitCost.text = products[currentIndex].unitPrice
            self.normalSell.text = products[currentIndex].retailPrice
            self.currentGP.text = products[currentIndex].gp
            self.caseQty.text = products[currentIndex].caseQty
            self.qtyText.text = products[currentIndex].getQty
            self.transitStock.text = products[currentIndex].transitStock
            self.abUVal.setOn(products[currentIndex].abUpdate, animated: true);
            if(products[currentIndex].abUpdate == true){
                aisleBayView.isHidden = false
                aisleTextField.text = products[currentIndex].ChangeAisle
                bayTextField.text = products[currentIndex].ChangeBay
            } else {
                aisleBayView.isHidden = true
                aisleTextField.text = "0"
                bayTextField.text = "0"
            }
            
            if(products[currentIndex].tickReq == true){
                TicketReqView.isHidden = false
                TickReqNum.text = products[currentIndex].tickReqNum
            } else {
                TicketReqView.isHidden = true
                TickReqNum.text = "1"
            }
            self.tickReqVal.setOn(products[currentIndex].tickReq, animated: true);
            var aisleBay = products[currentIndex].aisleBay.components(separatedBy: "-")
            if(aisleBay.indices.contains(0) && aisleBay.indices.contains(1)){
                if(aisleBay[0] == "99"){
                    aisleBay[0] = "??"
                }
                if(aisleBay[1] == "99"){
                    aisleBay[1] = "??"
                }
                if(aisleBay.count > 1){
                    self.aisle.setTitle("Aisle "+aisleBay[0] + " Bay " + aisleBay[1], for: .normal)
                }
            } else {
                self.aisle.setTitle("Aisle ??" + " Bay ??", for: .normal)
            }
            productImage.image = UIImage(named: "TicketsRequired.png")
            
            if(products[currentIndex].image != nil){
                productImage.image = products[currentIndex].image
            }
            //downloadImage(url: URL(string: "https://s3.amazonaws.com/imagesFarmerJacks/"+products[currentIndex].SKU)!)
            
            self.qtyText.selectAll(nil)
            self.qtyText.becomeFirstResponder()
        }
    }
    
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                if let image = UIImage(data: data){
                    self.productImage.image = image
                }
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func GetProducts(table: String, branch : String){
        var num = 0
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch+",Stocked,"+self.selSupplier+"&"+Constants.URLS.APIKEY
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                if(data == nil){
                    self.progress.hide()
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = ProductStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["aisleBay"] as? String {
                                struc.aisleBay = self.ValidateAisleBay(aisleBay: i)
                            }
                            if let i = item["caseQty"] as? String {
                                struc.caseQty = i
                            }
                            if let i = item["description"] as? String {
                                struc.description = i
                            }
                            if let i = item["gp"] as? String {
                                struc.gp = i
                            }
                            if let i = item["metrics"] as? String {
                                struc.metrics = i
                            }
                            if let i = item["qty"] as? String {
                                struc.qty = i
                            }
                            if let i = item["retailPrice"] as? String {
                                struc.retailPrice = i
                            }
                            if let i = item["saleDay"] as? String {
                                struc.saleDay = i
                            }
                            if let i = item["supplier"] as? String {
                                struc.supplier = i
                            }
                            if let i = item["unitPrice"] as? String {
                                struc.unitPrice = i
                            }
                            if let i = item["AvSales"] as? String {
                                struc.avSales = i
                            }
                            if let i = item["lastSales"] as? String {
                                struc.lastSales = i
                            }
                            if let i = item["transitStock"] as? String {
                                struc.transitStock = i
                            }
                            self.getDataFromUrl(url: URL(string: "https://s3.amazonaws.com/imagesFarmerJacks/"+struc.SKU)!) { (data, response, error)  in
                                guard let data = data, error == nil else { return }
                                DispatchQueue.main.async() { () -> Void in
                                    if let image = UIImage(data: data){
                                        struc.image = image
                                    }
                                }
                            }
                            self.products.append(struc)
                            num = num+1
                            
                        }
                    } else {
                        print("Error!!")
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.InitOrder()
                })
            }
            task.resume()

        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    //Validate Aisle and Bay
    func ValidateAisleBay(aisleBay: String) -> String{
        var value = aisleBay
        
        let letters = NSCharacterSet.letters
        
        let ab = aisleBay.components(separatedBy: "-")
        
        if(ab.indices.contains(1)){
            let range = ab[1].rangeOfCharacter(from: letters)
            let range2 = ab[0].rangeOfCharacter(from: letters)
            if (range != nil && range2 != nil) {
                value = "??-??"
            }
            
            if (range != nil) {
                value = "??-??"
            }
        } else {
            value = "??-??"
        }
        
        if(aisleBay == "99-99"){
            value = "??-??"
        }
        
        if(aisleBay.count > 5){
            value = "??-??"
        }
        
        if(aisleBay.count < 5){
            value = "??-??"
        }
        
        return value
    }
    
    func InitOrder(){
            if(UserDefaults.standard.bool(forKey: "Modify") == false){
                if(UserDefaults.standard.object(forKey: "Products") != nil){
                    if let data = UserDefaults.standard.object(forKey: "Products") as? [[String]] {
                        self.SetProducts(array: data)
                    }
                }
            } else {
                if(UserDefaults.standard.object(forKey: "ModifyOrder") != nil){
                    if let data = UserDefaults.standard.object(forKey: "ModifyOrder") as? [[String]] {
                        self.SetProducts(array: data)
                        saved = false
                    }
                }
            }
        self.SortProducts()
        self.CheckForBadAisleBay()
        self.SetAisleBayData()
        self.SetData()
        self.progress.hide()
        self.aisleBayButton.isHidden = false
        self.reviewButton.isHidden = false
        self.saveButton.isHidden = false
        self.mainView.isHidden = false
        self.SKU.isHidden = false
        self.supplier.isHidden = false
        currentView.isHidden = false
        self.picker.reloadAllComponents()
        if(!self.products.isEmpty){
            self.SetData()
        }
    }
    @IBOutlet var currentView: UIView!
    
    func activityIndicator() {
        self.view.addSubview(self.progress)
        self.aisleBayButton.isHidden = true
        self.reviewButton.isHidden = true
        self.saveButton.isHidden = true
        self.mainView.isHidden = true
        self.SKU.isHidden = true
        self.supplier.isHidden = true
        
        currentView.isHidden = true
    }
    
    func Save(){
        if(saved == false){
            saved = true
            SendUpdates()
            let preferences = UserDefaults.standard
        
            preferences.set(self.currentIndex, forKey: "save")
            
            do {
                preferences.set(try self.SaveProducts(), forKey: "Products")
                preferences.set(false, forKey: "Modify")
                preferences.synchronize()
            } catch let error as NSError{
                SendLog(msg: error.description )
            }
            Message(msg: "Successfully Saved")
        }
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "StoreCode")!.isEmpty){
            storeCode = preferences.string(forKey: "StoreCode")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            selSupplier = preferences.string(forKey: "Supplier")!
        }
        
        let save = preferences.integer(forKey: "save")
        currentIndex = save
        
        GetProducts(table: "Products", branch: branch)
        
        self.SetData()
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func CheckMessage() {
        let alert = UIAlertController(title: "Alert", message: "There is an unsaved order do you wish to save it?", preferredStyle: UIAlertController.Style.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.Save()
            self.navigationController?.popToRootViewController(animated: true)
        }
        let noAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func CheckModifyMessage() {
        let alert = UIAlertController(title: "Alert", message: "This will overwrite your order, do you wish to save?", preferredStyle: UIAlertController.Style.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.Save()
        }
        let noAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func SendOrder(aisleBayUpdate: String, ticketsRequired: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            print("Internet Connection Available!")
            let json: [String: Any] = ["ticketsRequired": ticketsRequired,
                                       "aisleBayUpdate": aisleBayUpdate,
                                       "order": "NULL",
                                       "apiKey": Constants.URLS.APIKEY2]
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/order");
            
            var request = URLRequest(url:myUrl!)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil
                {
                    return
                }
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        if(items["Message"] != nil){
                            msg = items["Message"] as! String
                        } else {
                            msg = "Error!"
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        }
        else{
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func SendLog(msg: String){
        if Reachability.isConnectedToNetwork() == true{
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy-HH:mm:ss"
            
            let now = formatter.string(from: date)
            
            let error = now+","+msg
            print("Internet Connection Available!")
            let json: [String: Any] = ["table": "ErrorLogs",
                                       "values": error,
                                       "apiKey": Constants.URLS.APIKEY2]
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/write");
            
            var request = URLRequest(url:myUrl!)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil
                {
                    //print("error=\(error)")
                    return
                }
                
                // You can print out response object
                //print("response = \(response)")
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let _ = json as? [String: Any] {
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        }
        else{
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func GetAisleBay(expiryDate: Double) -> String{
        let preferences = UserDefaults.standard
        
        var str = ""
        
        let branch = preferences.string(forKey: "Branch")!
        
        if(products.count > 0){
            for item in products{
                if(item.abUpdate == true){
                    str += branch+","
                    str += item.SKU+","
                    str += item.description+","
                    str += item.ChangeAisle+"-"+item.ChangeBay+","
                    str += "false,"
                    str += String(format:"%f", expiryDate)+";"
                }
            }
        }
        
        if(str == ""){
            str = "NULL"
        }
        
        return str
    }
    
    func GetTickets(expiryDate: Double) -> String{
        let preferences = UserDefaults.standard
        
        var str = ""
        
        let branch = preferences.string(forKey: "Branch")!
        
        if(products.count > 0){
            for item in products{
                if(item.tickReq == true){
                    str += branch+","
                    str += item.SKU+","
                    str += item.description+","
                    str += item.tickReqNum+","
                    str += item.ChangeAisle+"-"+item.ChangeBay+","
                    str += "false,"
                    str += String(format:"%f", expiryDate)+";"
                }
            }
        }
        if(str == ""){
            str = "NULL"
        }
        return str
    }
    
    func SendUpdates(){
        let expiryDate = Calendar.current.date(byAdding:
            .day, // updated this params to add hours
            value: 7,
            to: Date())
        
        let aislebay = GetAisleBay(expiryDate: expiryDate!.timeIntervalSince1970)
        let tickets = GetTickets(expiryDate: expiryDate!.timeIntervalSince1970)
        
        if(aislebay != "NULL" || tickets != "NULL"){
            SendOrder(aisleBayUpdate: aislebay, ticketsRequired: tickets)
        }
    }
    
    //Sort Products by ailse and bay then alpha
    func SortProducts(){
        self.products = self.products.sorted(by: { $0.aisleBay < $1.aisleBay })
        var current = ""
        var prod = [PrductsViewController.ProductStruct()]
        prod = []
        var array = [PrductsViewController.ProductStruct()]
        array = []
        for item in self.products{
            if(current == ""){
                current = item.aisleBay
            }
            
            if(current != item.aisleBay){
                current = item.aisleBay
                array = array.sorted(by: { $0.description < $1.description })
                prod.append(contentsOf: array)
                array = []
            }
            
            array.append(item)
        }
        prod.append(contentsOf: array)
        
        self.products = prod
    }

    func CheckForBadAisleBay(){
        var prod = [ProductStruct]()
        
        prod = products.filter { $0.aisleBay == "??-??" }
        products = products.filter { $0.aisleBay != "??-??" }
        
        products.append(contentsOf: prod)
    }
    
    func RefreshOrder(){
        let len = products.count
        for i in 0..<len{
            products[i].getQty = "0"
            products[i].abUpdate = false
            products[i].ChangeAisle = "0"
            products[i].ChangeBay = "0"
            products[i].tickReq = false
            products[i].tickReqNum = "0"
        }
    }
}

