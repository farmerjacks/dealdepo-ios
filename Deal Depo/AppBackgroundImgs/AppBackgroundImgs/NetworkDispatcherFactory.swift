//
//  NetworkDispatcherFactory.swift
//  FarmerJacksApp
//
//  Created by Ahmed Sadiq on 9/7/19.
//  Copyright © 2019 SCS. All rights reserved.
//

import UIKit

public class NetworkDispatcherFactory {
    
    private var environment: Environment
    
    required public init(environment: Environment) {
        self.environment = environment
    }
    
    //func makeNetworkProvider() -> Dispatcher {
        //return NetworkDispatcher(self.environment)
    //}
}
