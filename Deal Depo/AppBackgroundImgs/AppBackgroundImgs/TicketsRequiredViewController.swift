//
//  TicketsRequiredViewController.swift
//  AppBackgroundImgs
//
//  Created by Admin on 10/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class TicketsRequiredViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var TicketsRequiredTableView: UITableView!
    
    var branch = "BUTLER"
    var supplier = "Deal Depot"
    
    //Ticket required struct
    struct TicketReqStruct {
        var SKU: String = "0000"
        var description: String = "0000"
        var qty: String = "0"
        var checked: Bool = false
    }
    
    var tickets = [TicketReqStruct]()
    
    var SKU = [String]()
    var productNames = [String]()
    var qty = [String]()
    var active = [Bool]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUserData()
        GetData(table: "TicketsRequired", branch : branch)
        
        TicketsRequiredTableView.dataSource = self
        TicketsRequiredTableView.delegate = self
        
        
        let nibName = UINib(nibName: "TicketsTableCell", bundle: nil)
       TicketsRequiredTableView.register(nibName, forCellReuseIdentifier: "TicketsCell")
        // Do any additional setup after loading the view.
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            supplier = preferences.string(forKey: "Supplier")!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SKU.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TicketsRequiredTableView.dequeueReusableCell(withIdentifier: "TicketsCell", for: indexPath) as! TicketsTableCell
        cell.commonInit(SKU[indexPath.item], tempProdName: productNames[indexPath.item], tempQty: qty[indexPath.item], tempActive: active[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetData(){
        SKU = [String]()
        productNames = [String]()
        qty = [String]()
        active = [Bool]()
        for item in tickets{
            SKU.append(item.SKU)
            productNames.append(item.description)
            qty.append(item.qty)
            active.append(item.checked)
        }
        TicketsRequiredTableView.reloadData()
    }
    
    
    func GetData(table: String, branch : String){
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let url = URL(string: Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = TicketReqStruct()
                            if let i = item["SKU"] as? String {
                                struc.SKU = i
                            }
                            if let i = item["qty"] as? String {
                                struc.qty = i
                            }
                            if let i = item["flag"] as? Bool {
                                struc.checked = i
                            }
                            if let i = item["description"] as? String {
                                struc.description = i
                            }
                            
                            self.tickets.append(struc)
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.SetData()
                            self.progress.hide()
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    //Activity Indiciator
    var progress = ProgressHUD(text: "Sending")
    
    func SendData(tickets: String){
        var msg = ""
        if Reachability.isConnectedToNetwork() == true{
            let json: [String: Any] = ["tickets": tickets,
                                       "table": "TicketRequired"]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let myUrl = URL(string: Constants.URLS.BASE+"/ReadWrite");
            
            var request = URLRequest(url:myUrl!)
            
            request.httpMethod = "POST"// Compose a query string
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                self.progress.show()
                if error != nil
                {
                    //print("error=\(error)")
                    return
                }
                
                // You can print out response object
                //print("response = \(response)")
                
                //Let's convert response sent from a server side script to a json object
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json as? [String: Any] {
                        self.progress.hide()
                        msg = items["Message"] as! String
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.progress.hide()
                            self.Message(msg: msg)
                        })
                    } else {
                        print("Error!!")
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func activityIndicator() {
        progress = ProgressHUD(text: Constants.ERRORS.LOADING_TICKETS)
        progress.show()
    }
}
