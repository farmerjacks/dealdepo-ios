//
//  HomeViewController.swift
//  FarmerJacksApp
//
//  Created by Daniel Kearsley on 19/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit
//import NumPad

class HomeEmpViewController: UIViewController {
    var weekday = ["SU", "M", "TU", "W", "TH", "F", "SA"]
    let uiBusy = UIActivityIndicatorView(style: .white)
    //Components
    //ADMIN
    @IBOutlet weak var searchProducts1: UIButton!
    @IBOutlet weak var notifications: UIButton!
    
    //EMPLOYEE
    @IBOutlet weak var createOrder: UIButton!
    @IBOutlet weak var productChange: UIButton!
    @IBOutlet weak var ab: UIButton!
    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var orderHistory: UIButton!
    
    @IBOutlet weak var notifyButton: UIButton!
    
    //Notifications
    var msg = UserDefaults.standard.object(forKey: "messages") as? [String]
    var sub = UserDefaults.standard.object(forKey: "subjects") as? [String]
    var intervals = UserDefaults.standard.object(forKey: "intervals") as? [String]
    var count = UserDefaults.standard.integer(forKey: "msgCount")
    
    var orderData = [OrderHistoryViewController.OrderStruct()]
    
    var branch = ""
    var supplier = ""
    
    var notified = false
    
    @IBOutlet weak var spButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let prefs = UserDefaults.standard
        var priv = ""
        
        if(!prefs.string(forKey: "privilege")!.isEmpty){
            priv = prefs.string(forKey: "privilege")!
        }
        
        createOrder.isEnabled = false
        notified = UserDefaults.standard.bool(forKey: "notified")
        if(notified == false){
            Notify()
        }
        
        uiBusy.hidesWhenStopped = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: uiBusy)
        SetUserData()
        //Check Order
        if(priv != "admin"){
            GetData(table: "Orders", branch : branch)
        }
        
        if(priv == "admin"){
            createOrder.isHidden = true
            productChange.isHidden = true
            ab.isHidden = true
            upButton.isHidden = true
            orderHistory.isHidden = true
            spButton.isHidden = true
            notifyButton.isHidden = true
            
            searchProducts1.isHidden = false
            notifications.isHidden = false
        } else {
            createOrder.isHidden = false
            productChange.isHidden = false
            ab.isHidden = false
            upButton.isHidden = false
            orderHistory.isHidden = false
            spButton.isHidden = false
            notifyButton.isHidden = false
            
            searchProducts1.isHidden = true
            notifications.isHidden = true
        }
        if(msg == nil){
            count = 0
        }
        notifyButton.setTitle("Today's Notifications - "+String(count), for: .normal)
        
        //let numPad = DefaultNumPad()
        //self.view.addSubview(numPad)
        
        self.addShadow(viewTop: searchProducts1)
        self.addShadow(viewTop: notifications)
        self.addShadow(viewTop: createOrder)
        self.addShadow(viewTop: productChange)
        self.addShadow(viewTop: ab)
        self.addShadow(viewTop: upButton)
        self.addShadow(viewTop: spButton)
        self.addShadow(viewTop: orderHistory)
        self.addShadow(viewTop: notifyButton)
        
    }
    
    func SetUserData(){
        let preferences = UserDefaults.standard
        
        if(!preferences.string(forKey: "Branch")!.isEmpty){
            branch = preferences.string(forKey: "Branch")!
        }
        if(!preferences.string(forKey: "Supplier")!.isEmpty){
            supplier = preferences.string(forKey: "Supplier")!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let prefs = UserDefaults.standard
        var priv = ""
        
        if(!prefs.string(forKey: "privilege")!.isEmpty){
            priv = prefs.string(forKey: "privilege")!
        }
        
        //Check Order
        if(priv != "admin"){
            GetData(table: "Orders", branch : branch)
        }
    }
    
    @IBAction func NotifyAction(_ sender: Any) {
        Notify()
    }
    
    func Notify(){
        if(msg != nil){
            if((msg?.count)! > 0){
                var alerts = [UIAlertController]()
                let date = Date()
                let calendar = Calendar.current
                
                let day = calendar.component(.weekday, from: date)
                let index = day-1
                
                let len = msg?.count
                if(len! > 0){
                    for i in 0..<len! {
                        var intv = intervals?[i].components(separatedBy: "-")
                        if(intv?[index] == "true"){
                            let alert = UIAlertController(title: sub?[i], message: msg?[i], preferredStyle: UIAlertController.Style.alert)
                            
                            alerts.append(alert)
                        }
                    }
                }
                var action = UIAlertAction()
                let length = alerts.count
                var nextIndex = 0
                for i in 0..<length {
                    action = UIAlertAction(title: "OK", style: .default) { (action) in
                        nextIndex = i+1
                        if(alerts.indices.contains(nextIndex) == true){
                            self.present(alerts[nextIndex], animated: true, completion: nil)
                        } else {
                            //self.dismiss(animated: true, completion: nil)
                        }
                    }
                    alerts[i].addAction(action)
                }
                if(alerts.count > 0){
                    self.present(alerts[0], animated: true, completion: nil)
                }
            }
        }
        UserDefaults.standard.set(true, forKey: "notified")
    }
    
    //Resume Order
    func SaveProducts() -> [[String]]{
        var array = [[String]]()
        for item in orderData{
            if(Int(item.qty)! > 0){
                var prod = [String]()
                prod.append(item.SKU)
                prod.append(item.qty)
                prod.append("false")
                prod.append("0")
                prod.append("0")
                prod.append("0")
                prod.append("false")
                prod.append(item.orderDay)
                
                array.append(prod)
            }
        }
        return array
    }
    
    //var progress = ProgressHUD(text: "Checking Orders")
    
    func GetData(table: String, branch : String){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy-HH:mm:ss"
        
        var now = formatter.string(from: date)
        
        now = now.components(separatedBy: "-")[0]
        if Reachability.isConnectedToNetwork() == true{
            activityIndicator()
            let urlStr = Constants.URLS.BASE+Constants.URLS.METHOD+table+","+branch+"&"+Constants.URLS.APIKEY
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard data != nil else {
                    print("Data is empty")
                    return
                }
                if(data == nil){
                    self.createOrder.isEnabled = true
                    self.uiBusy.stopAnimating()
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    if (httpResponse.statusCode == 3840){
                        self.Message(msg: "No Response")
                    }
                }
                
                do {
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let items = json["Items"] as? [[String: Any]] {
                        for item in items {
                            var struc = OrderHistoryViewController.OrderStruct()
                            var orderDate = ""
                            if let i = item["date"] as? String {
                                orderDate = i.components(separatedBy: "-")[0]
                            }
                            
                            if(orderDate != ""){
                                if(orderDate == now){
                                    if let i = item["SKU"] as? String {
                                        struc.SKU = i
                                    }
                                    if let i = item["description"] as? String {
                                        struc.description = i
                                    }
                                    if let i = item["date"] as? String {
                                        struc.date = i
                                    }
                                    if let i = item["qty"] as? String {
                                        struc.qty = i
                                    }
                                    if let i = item["orderDay"] as? String {
                                        struc.orderDay = i
                                    }
                                    self.orderData.append(struc)
                                }
                            }
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.uiBusy.stopAnimating()
                            let prods = self.SaveProducts()
                            if(!prods.isEmpty){
                                let date = Date()
                                let calendar = Calendar.current
                                
                                let day = calendar.component(.weekday, from: date)
                                let index = day-1
                                
                                if self.orderData.firstIndex(where: { $0.orderDay == self.weekday[index] }) != nil {
                                    self.createOrder.setTitle("Resume Order", for: .normal)
                                } else {
                                    self.createOrder.setTitle("Create Order", for: .normal)
                                }
                                
                                if(UserDefaults.standard.object(forKey: "Products") == nil){
                                    UserDefaults.standard.set(prods, forKey: "Products")
                                    UserDefaults.standard.synchronize()
                                    //self.createOrder.setTitle("Resume Order", for: .normal)
                                } else {
                                    //self.createOrder.setTitle("Resume Order", for: .normal)
                                }
                            }
                            self.createOrder.isEnabled = true
                        })
                    }
                    
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
            }
            task.resume()
        } else {
            self.Message(msg: Constants.ERRORS.NO_INTERNET_CONNECTION)
        }
    }
    
    func Message(msg: String) {
        let alert = UIAlertController(title: "Response", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func activityIndicator() {
        uiBusy.startAnimating()
        
        //self.view.addSubview(self.progress)
        //progress.show()
    }
    func addShadow(viewTop: UIButton)
    {
        viewTop.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewTop.layer.shadowColor = UIColor.lightGray.cgColor
        viewTop.layer.shadowOpacity = 1
        viewTop.layer.shadowRadius = 5
        viewTop.layer.masksToBounds = false
    }
}
