//
//  OldOrderReviewViewController.swift
//  AppBackgroundImgs
//
//  Created by Admin on 11/7/17.
//  Copyright © 2017 SCS. All rights reserved.
//

import UIKit

class OldOrderReviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    var weekday = ["SU", "M", "TU", "W", "TH", "F", "SA"]
    var orderDay = 0
    @IBOutlet weak var curOrder: UILabel!
    
    var orderData = [OrderHistoryViewController.OrderStruct()]
    
    var SKU = [String]()
    var productNames = [String]()
    var qty = [String]()
    
    @IBOutlet weak var oldOrderReviewTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.weekday, from: date)
        let index = day-1
        
        let hour = calendar.component(.hour, from: date)
        
        let currentDay = weekday[index]
        
        if(currentDay == weekday[orderDay] || currentDay == weekday[orderDay+1]){
            if(hour < 15){
                self.navigationItem.setHidesBackButton(true, animated:true);
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "MODIFY", style: .plain, target: self, action: #selector(ModifyOrder))
            }
        }
        
        SortProducts()
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "BACK", style: .plain, target: self, action: #selector(back))
        
        oldOrderReviewTableView.dataSource = self
        oldOrderReviewTableView.delegate = self
        
        if(!orderData.isEmpty){
            SetData()
        }
        let nibName = UINib(nibName: "OldOrderCell", bundle: nil)
        oldOrderReviewTableView.register(nibName, forCellReuseIdentifier: "OldCell")
        
        // Do any additional setup after loading the view.
    }
    
    @objc func back(){
        navigationController!.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SKU.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = oldOrderReviewTableView.dequeueReusableCell(withIdentifier: "OldCell", for: indexPath) as! OldOrderCell
        cell.commonInit(SKU[indexPath.item], tempProdName: productNames[indexPath.item], tempQty: qty[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetData(){
        SKU = [String]()
        productNames = [String]()
        qty = [String]()
        for item in orderData{
            SKU.append(item.SKU)
            productNames.append(item.description)
            qty.append(item.qty)
            curOrder.text = item.date
        }
        oldOrderReviewTableView.reloadData()
    }
    
    @objc func ModifyOrder(){
        let products = self.SaveProducts()
        UserDefaults.standard.set(true, forKey: "Modify")
        UserDefaults.standard.set(products, forKey: "ModifyOrder")
        UserDefaults.standard.synchronize()
        
        if(!products.isEmpty){
            performSegue(withIdentifier: "ModifyOrder", sender: self)
        }
    }
    
    func SaveProducts() -> [[String]]{
        var array = [[String]]()
        for item in orderData{
            if(Int(item.qty)! > 0){
                var prod = [String]()
                prod.append(item.SKU)
                prod.append(item.qty)
                prod.append("false")
                prod.append("0")
                prod.append("0")
                prod.append("0")
                prod.append("false")
                prod.append(item.orderDay)
                
                array.append(prod)
            }
        }
        
        return array
    }
    
    //Sort Products by ailse and bay then alpha
    func SortProducts(){
        self.orderData = self.orderData.sorted(by: { $0.aisleBay < $1.aisleBay })
        var current = ""
        var prod = [OrderHistoryViewController.OrderStruct()]
        prod = []
        var array = [OrderHistoryViewController.OrderStruct()]
        array = []
        for item in self.orderData{
            if(current == ""){
                current = item.aisleBay
            }
            
            if(current != item.aisleBay){
                current = item.aisleBay
                array = array.sorted(by: { $0.description < $1.description })
                prod.append(contentsOf: array)
                array = []
            }
            
            array.append(item)
        }
        prod.append(contentsOf: array)
        
        self.orderData = prod
    }
}
